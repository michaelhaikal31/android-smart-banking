package products.dak.com.smartbank.fragments.Product;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import products.dak.com.smartbank.R;
import products.dak.com.smartbank.interfaces.SnackBar;

public class DepositoBerjangkaFragment extends Fragment  {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    private SnackBar callback;

    public DepositoBerjangkaFragment() {
        // Required empty public constructor
    }

    public static DepositoBerjangkaFragment newInstance(String param1, String param2) {
        DepositoBerjangkaFragment fragment = new DepositoBerjangkaFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

       callback.showSnackBar(true, "Deposito Berjangka");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_tabungan_berjangka, container, false);

        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            callback = (SnackBar)context;

        }catch (Exception e){
            System.out.println("DAS"+e.toString());
        }
    }
}