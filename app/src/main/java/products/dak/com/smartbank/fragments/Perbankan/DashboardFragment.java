package products.dak.com.smartbank.fragments.Perbankan;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import products.dak.com.smartbank.R;
import products.dak.com.smartbank.Sessions.UserSession;
import products.dak.com.smartbank.activities.main.SuperActivity;
import products.dak.com.smartbank.adapters.MainSliderAdapter;
import products.dak.com.smartbank.interfaces.ApiService;
import products.dak.com.smartbank.models.Tabungan.Saldo;
import products.dak.com.smartbank.services.GlideImageLoadingService;
import products.dak.com.smartbank.services.RetrofitService;
import products.dak.com.smartbank.utils.Constant;
import products.dak.com.smartbank.utils.TripleDes;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ss.com.bannerslider.Slider;


public class DashboardFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    private ImageButton btnSaldo,btnMutasi,btnTransfer,btnIsiPulsa,btnPostPaid,btnPln;
    private JSONObject paramObject;
    private TripleDes tripleDesJson;
    private String encryptedBody;
    private TextView saldo,userName,no_rek;
    private ProgressDialog progressDialog;
    private ProgressDialog pd;
    private GlideImageLoadingService glideImageLoadingService;
    private Slider slider;

    private BottomSheetBehavior sheetBehavior;

    public DashboardFragment() {
    }

    public static DashboardFragment newInstance(String param1, String param2) {
        DashboardFragment fragment = new DashboardFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_dashboard, container, false);
        glideImageLoadingService = new GlideImageLoadingService(getActivity());
        Slider.init(glideImageLoadingService);
        slider = v.findViewById(R.id.banner_slider1);
        slider.setAdapter(new MainSliderAdapter());
        btnSaldo = v.findViewById(R.id.btnSaldo);
        btnMutasi = v.findViewById(R.id.btnMutasi);
        btnTransfer = v.findViewById(R.id.btnTransfer);
        btnIsiPulsa = v.findViewById(R.id.btnIsiPulsa);
        btnPostPaid = v.findViewById(R.id.btnPostPaid);
        btnPln = v.findViewById(R.id.btnPln);



        btnMutasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(getActivity(), SuperActivity.class);
                i.putExtra("menu","mutasi");
                startActivity(i);
                getActivity().overridePendingTransition( R.anim.sliding_in_up, R.anim.sliding_out_up );
            }
        });
        btnTransfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), SuperActivity.class);
                i.putExtra("menu","transfer");
                startActivity(i);
                getActivity().overridePendingTransition( R.anim.sliding_in_up, R.anim.sliding_out_up );
            }
        });
        btnIsiPulsa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), SuperActivity.class);
                i.putExtra("menu","isi_pulsa");
                startActivity(i);
                getActivity().overridePendingTransition( R.anim.sliding_in_up, R.anim.sliding_out_up );
            }
        });
        btnPostPaid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), SuperActivity.class);
                i.putExtra("menu","post_paid");
                startActivity(i);
                getActivity().overridePendingTransition( R.anim.sliding_in_up, R.anim.sliding_out_up );
            }
        });
        btnPln.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), SuperActivity.class);
                i.putExtra("menu","pln");
                startActivity(i);
                getActivity().overridePendingTransition( R.anim.sliding_in_up, R.anim.sliding_out_up );
            }
        });
        btnSaldo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Please wait");
                progressDialog.show();
                check_saldo();

                //cek();
            }
        });
        /*View vButtonSheet = v.findViewById(R.id.buttom_sheet1);
       sheetBehavior =BottomSheetBehavior.from(vButtonSheet);
        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int i) {
                switch (i) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                        //btnBottomSheet.setText("Close Sheet");
                    }
                    break;
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        //btnBottomSheet.setText("Expand Sheet");
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }


            }

            @Override
            public void onSlide(@NonNull View view, float v) {

            }
        });*/
        return  v;
    }


    private void check_saldo(){
        final String key = Constant.getKey(24);
        ApiService apiInterface = RetrofitService.getRetrofit(key).create(ApiService.class);
        paramObject = new JSONObject();
        final UserSession userSession = new UserSession();
        try{
            tripleDesJson = new TripleDes(key);
            paramObject.put("msisdn",userSession.getMsiSdn(getActivity()));
            paramObject.put("jenis_tabungan","tabungan bpr modern");
            encryptedBody = tripleDesJson.encrypt(paramObject.toString());
        }catch (JSONException e){

        }catch (Exception e){

        }
        System.out.println("asd "+paramObject.toString());
        Call<String> call = apiInterface.cek_saldo(encryptedBody);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try{
                    //Toast.makeText(getActivity(),response.body().toString(),Toast.LENGTH_SHORT).show();
                    Saldo valueSaldo = new Gson().fromJson(tripleDesJson.decrypt(response.body()),Saldo.class);
                    if(valueSaldo.rm.equalsIgnoreCase("Success")){
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        LayoutInflater inflater = getLayoutInflater();
                        View view =inflater.inflate(R.layout.dialog_saldo,null);
                        userName = view.findViewById(R.id.txtDialogNamaUser);
                        saldo = view.findViewById(R.id.txtDialogSaldo);
                        no_rek = view.findViewById(R.id.txtDialogNoRek);
                        saldo.setText("Rp."+valueSaldo.data.getBalance());
                        userName.setText(valueSaldo.data.getName());
                        no_rek.setText(valueSaldo.data.getRekening_no());
                        alert.setView(view);
                        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                return;
                            }
                        });
                        final AlertDialog dialog = alert.create();
                        dialog.show();
                    }else{
                        Snackbar.make(getView(),valueSaldo.rm,Snackbar.LENGTH_SHORT).show();
                    }

                }catch (Exception e){
                    Snackbar.make(getView(),"Cannot processing server's response !",Snackbar.LENGTH_SHORT).show();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Snackbar.make(getView(),"Oops, Something wrong please try again later",Snackbar.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }





}
