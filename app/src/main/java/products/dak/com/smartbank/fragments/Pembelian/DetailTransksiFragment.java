package products.dak.com.smartbank.fragments.Pembelian;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import products.dak.com.smartbank.R;
import products.dak.com.smartbank.adapters.ResponAdapter;
import products.dak.com.smartbank.utils.ConstantTransaction;
import products.dak.com.smartbank.utils.Respon;

public class DetailTransksiFragment extends Fragment {
    private static final String ARG_PARAM1 = ConstantTransaction.BUNDLE_RM_RESULT;
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private String rmresult;
    private ListView listView;
    View view;
    private List<String> listitem =new ArrayList<>();
    private Button btnSelesai;
    private ArrayList<Respon> responArrayList = new ArrayList<>();

    public DetailTransksiFragment() {
        // Required empty public constructor
    }

   public static DetailTransksiFragment newInstance(String param1, String param2) {
        DetailTransksiFragment fragment = new DetailTransksiFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_detail_transksi, container, false);
        InitUi();
        return view;
    }

    private void InitUi() {

        try {
            JSONObject response = new JSONObject(mParam1);

            listitem.add(response.getString("msisdn"));
            listitem.add(response.getString("denom"));
            listitem.add(response.getString("product_code"));
            listitem.add(response.getString("ref"));
            listitem.add(response.getString("rm"));
            listView = view.findViewById(R.id.listViewP);

            String[] header = new String[]{"msisdn", "denom", "product_code","reff","status"};
            for(int i=0;i<header.length;i++){
                responArrayList.add(new Respon(header[i], listitem.get(i)));
            }
            ResponAdapter responAdapter = new ResponAdapter(getActivity(), responArrayList);
            listView.setAdapter(responAdapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        btnSelesai = view.findViewById(R.id.btnSelesai);
        btnSelesai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               getActivity().finish();
            }
        });
    }

}
