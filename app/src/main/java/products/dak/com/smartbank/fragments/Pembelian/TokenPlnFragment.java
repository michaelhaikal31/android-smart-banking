package products.dak.com.smartbank.fragments.Pembelian;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import products.dak.com.smartbank.R;
import products.dak.com.smartbank.fragments.Login.Login;


public class TokenPlnFragment extends Fragment {

    private static final String ARG_PARAM1 = "step";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param2";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String mParam3;
    private TextInputLayout textInputLayout1,textInputLayout2;
    private TextView txtKeterangan,txtNama,txtKwh,txtNominal2;
    private EditText txtNominal,txtNoMeter,txtPin;
    private Button btnBeliToken;
    private boolean verified;

    public TokenPlnFragment() {
        // Required empty public constructor
    }

    public static TokenPlnFragment newInstance(String param1, String param2,String param3) {
        TokenPlnFragment fragment = new TokenPlnFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        args.putString(ARG_PARAM3,param3);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            mParam3 = getArguments().getString(ARG_PARAM3);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = null;
        if(mParam1.equalsIgnoreCase("2")){
            v = inflater.inflate(R.layout.fragment_token_pln_step_two, container, false);
            txtNama = v.findViewById(R.id.txtNamaUser);
            txtKwh = v.findViewById(R.id.txtKwh);
            txtNominal = v.findViewById(R.id.txtNominalTokenPln);
            txtPin = v.findViewById(R.id.txtPinTokenPln);
            btnBeliToken = v.findViewById(R.id.btnBeliTokenPln2);
            btnBeliToken.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    beli_token();
                }
            });
        }else {
            v = inflater.inflate(R.layout.fragment_token_pln_step_one, container, false);
            textInputLayout1 = v.findViewById(R.id.textInputLayout2);
            txtKeterangan = v.findViewById(R.id.txtKeteranganPln);
            txtNoMeter = v.findViewById(R.id.txtNoMeterPln);
            btnBeliToken = v.findViewById(R.id.btnBeliTokenPln1);
            txtKeterangan.setVisibility(View.INVISIBLE);
            textInputLayout1.setVisibility(View.INVISIBLE);
            btnBeliToken.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (verified) {
                        enter_pin();
                    } else {
                        check_no_meter();
                    }
                }
            });
        }
        return v;
    }

    private void check_no_meter(){
        txtKeterangan.setVisibility(View.VISIBLE);
        textInputLayout1.setVisibility(View.VISIBLE);
        verified=true;
    }

    private void enter_pin(){
        Login login = new Login();
        Bundle extras  = new Bundle();
        extras.putString("jenis","pln");
        login.setArguments(extras);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.frame_layout_super,login);
        ft.commit();
    }

    private void beli_token(){
        TokenPlnFragment tokenPlnFragment = new TokenPlnFragment();
        Bundle extras  = new Bundle();
        extras.putString("step","2");
        //extras.putString("nama",txtNoHandphone.getText().toString());
        tokenPlnFragment.setArguments(extras);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.frame_layout_super,tokenPlnFragment);
        ft.commit();

    }
}
