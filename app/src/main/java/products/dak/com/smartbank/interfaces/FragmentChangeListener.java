package products.dak.com.smartbank.interfaces;

import android.support.v4.app.Fragment;

public interface FragmentChangeListener {
     void replaceFragment(Fragment fragment);
    boolean onFragmentBackPressed();

}
