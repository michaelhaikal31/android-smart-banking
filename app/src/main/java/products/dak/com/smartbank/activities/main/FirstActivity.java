package products.dak.com.smartbank.activities.main;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import products.dak.com.smartbank.R;
import products.dak.com.smartbank.fragments.Register.Register2Fragment;
import products.dak.com.smartbank.fragments.SplashScreenFragment;
import products.dak.com.smartbank.interfaces.FragmentChangeListener;

public class FirstActivity extends AppCompatActivity {
    private FragmentTransaction fragmentTransaction;
    private FragmentManager fragmentManager;
    private ImageView logo;
    private int page;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        // getSupportActionBar().setElevation(0);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setLogo(R.drawable.logo_1);

        System.out.println("asd");

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
            window.setNavigationBarColor(Color.BLACK);
        }

        /*
        Login login = new Login();
        Bundle extras  = new Bundle();
        extras.putString("jenis","1");
        login.setArguments(extras);
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.frame_layout1,login);
        ft.commit();
        */

        SplashScreenFragment splashScreenFragment = new SplashScreenFragment();
        Bundle extras = new Bundle();
        extras.putString("step", "1");
        splashScreenFragment.setArguments(extras);
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_layout1, splashScreenFragment);
        fragmentTransaction.commit();


        page = 1;
        logo = findViewById(R.id.logo);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
            /*
            SplashScreenFragment splashScreenFragment = new SplashScreenFragment();
            Bundle extras  = new Bundle();
            extras.putString("step","2");
            splashScreenFragment.setArguments(extras);
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.frame_layout1,splashScreenFragment);
            ft.commit();
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            page=1;
            */
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frame_layout1);
        if (!(fragment instanceof FragmentChangeListener) || !((FragmentChangeListener) fragment).onFragmentBackPressed()) {
            super.onBackPressed();
        }
    }

    /*
    public void animteTitle() {
        ConstraintLayout rl = (ConstraintLayout)findViewById(R.id.constraint);
        float r = DPtoPX(100);
        float mScreenWidth = rl.getWidth() / 2; // DPtoPX(100);
        float mScreenHeight = rl.getHeight() / 2;// DPtoPX(100);


        TranslateAnimation animation = new TranslateAnimation(0.0f, mScreenWidth/2, 0.0f, mScreenHeight/2); // new TranslateAnimation (float fromXDelta,float toXDelta, float fromYDelta, float toYDelta)

        animation.setDuration(450); // animation duration, change accordingly
        animation.setRepeatCount(0); // animation repeat count
        animation.setFillAfter(true);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Intent i = new Intent(FirstActivity.this,MainActivity.class);

                startActivity(i);
                finish();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        //logo.startAnimation(animation);
    }
    float DPtoPX(float dp) {
        Resources r = getResources();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics());
    }

    */


}
