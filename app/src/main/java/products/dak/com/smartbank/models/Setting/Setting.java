package products.dak.com.smartbank.models.Setting;

public class Setting {

public class changePin{
    public String old_pin;
    public String rc;
    public String conf_new_pin;
    public String new_pin;
    public String rm;
    public String msisdn;

    public changePin() {
    }

    public changePin(String old_pin, String rc, String conf_new_pin, String new_pin, String rm, String msisdn) {
        this.old_pin = old_pin;
        this.rc = rc;
        this.conf_new_pin = conf_new_pin;
        this.new_pin = new_pin;
        this.rm = rm;
        this.msisdn = msisdn;
    }

    public String getOld_pin() {
        return old_pin;
    }

    public String getRc() {
        return rc;
    }

    public String getConf_new_pin() {
        return conf_new_pin;
    }

    public String getNew_pin() {
        return new_pin;
    }

    public String getRm() {
        return rm;
    }

    public String getMsisdn() {
        return msisdn;
    }
}

public class changeMsisdn {
    public String rc;
    public String new_msisdn;
    public String rm;
    public String msisdn;


    public String getRc() {
        return rc;
    }

    public String getNew_msisdn() {
        return new_msisdn;
    }

    public String getRm() {
        return rm;
    }

    public String getMsisdn() {
        return msisdn;
    }
}
}

