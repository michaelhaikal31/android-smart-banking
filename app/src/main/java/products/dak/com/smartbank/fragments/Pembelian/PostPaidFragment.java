package products.dak.com.smartbank.fragments.Pembelian;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import products.dak.com.smartbank.R;
import products.dak.com.smartbank.fragments.Login.Login;

public class PostPaidFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private static final String ARG_PARAM4 = "param4";
    private static final String ARG_PARAM5 = "param5";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String mParam3;
    private String mParam4;
    private String mParam5;
    private TextView text1,text2,text3,text4,txtNoMeteran,txtTagihan,txtPeriode;
    private TextInputLayout textLayout1;
    private EditText txtPin;
    private boolean verified;
    private Button btnBayar;


    public PostPaidFragment() {
        // Required empty public constructor
    }

    public static PostPaidFragment newInstance(String param1, String param2, String param3, String param4, String param5) {
        PostPaidFragment fragment = new PostPaidFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        args.putString(ARG_PARAM3, param3);
        args.putString(ARG_PARAM4, param4);
        args.putString(ARG_PARAM5, param5);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            mParam3 = getArguments().getString(ARG_PARAM3);
            mParam4 = getArguments().getString(ARG_PARAM4);
            mParam5 = getArguments().getString(ARG_PARAM5);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_post_paid, container, false);
        text1 = v.findViewById(R.id.textView1);
        text2 = v.findViewById(R.id.textView2);
        text3 = v.findViewById(R.id.textView3);
        text4 = v.findViewById(R.id.textView4);
        txtNoMeteran = v.findViewById(R.id.txtViewNoMeteran);
        txtTagihan = v.findViewById(R.id.txtJumlahTagihan);
        txtPeriode = v.findViewById(R.id.txtPeriode);
        textLayout1 = v.findViewById(R.id.txtLayoutPostPaid);
        txtPin = v.findViewById(R.id.txtPinPostPaid);
        btnBayar = v.findViewById(R.id.btnBayarPostPaid);
        text1.setVisibility(View.INVISIBLE);
        text2.setVisibility(View.INVISIBLE);
        text3.setVisibility(View.INVISIBLE);
        text4.setVisibility(View.INVISIBLE);
        txtNoMeteran.setVisibility(View.INVISIBLE);
        txtTagihan.setVisibility(View.INVISIBLE);
        txtPeriode.setVisibility(View.INVISIBLE);
        textLayout1.setVisibility(View.INVISIBLE);
        txtPin.setVisibility(View.INVISIBLE);
        btnBayar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(verified){
                    bayar();
                }else{
                    check_no_meteran();
                }
            }
        });

        return v;
    }

    private void bayar(){
        Login login = new Login();
        Bundle extras  = new Bundle();
        extras.putString("jenis","post_paid");
        login.setArguments(extras);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.frame_layout_super,login);
        ft.commit();
    }

    private void check_no_meteran(){
        text1.setVisibility(View.VISIBLE);
        text2.setVisibility(View.VISIBLE);
        text3.setVisibility(View.VISIBLE);
        text4.setVisibility(View.VISIBLE);
        txtNoMeteran.setVisibility(View.VISIBLE);
        txtTagihan.setVisibility(View.VISIBLE);
        txtPeriode.setVisibility(View.VISIBLE);
        textLayout1.setVisibility(View.VISIBLE);
        txtPin.setVisibility(View.VISIBLE);
        verified=true;
    }
}
