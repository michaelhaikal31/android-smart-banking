package products.dak.com.smartbank.fragments.Register;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import at.nineyards.anyline.AnylineDebugListener;
import at.nineyards.anyline.core.RunFailure;
import at.nineyards.anyline.modules.ocr.AnylineOcrConfig;
import io.anyline.plugin.ScanResult;
import io.anyline.plugin.ScanResultListener;
import io.anyline.plugin.ocr.OcrScanResult;
import io.anyline.plugin.ocr.OcrScanViewPlugin;
import io.anyline.view.BaseScanViewConfig;
import io.anyline.view.ScanView;
import io.anyline.view.ScanViewPluginConfig;
import products.dak.com.smartbank.R;
import products.dak.com.smartbank.Sessions.UserSession;
import products.dak.com.smartbank.interfaces.OnBackPressed;
import products.dak.com.smartbank.utils.Constant;

public class ScanKtpFragment extends Fragment implements AnylineDebugListener {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;
    private ScanView scanView;
    private TextView txtclose;

    public ScanKtpFragment() {
        // Required empty public constructor
    }

    public static ScanKtpFragment newInstance(String param1, String param2) {
        ScanKtpFragment fragment = new ScanKtpFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_scan_ktp, container, false);
        scanView = view.findViewById(R.id.scanview);
        AnylineOcrConfig anylineOcrConfig = new AnylineOcrConfig();
        anylineOcrConfig.setLanguages("USNr.any");
        anylineOcrConfig.setCharWhitelist("1234567890");
        anylineOcrConfig.setScanMode(AnylineOcrConfig.ScanMode.AUTO);
        anylineOcrConfig.setValidationRegex("^-?\\d{16}$");
        scanView.setScanConfig("iban_view_config.json");
        OcrScanViewPlugin scanViewPlugin = new OcrScanViewPlugin(getContext(), getString(R.string.anyline_license_key), anylineOcrConfig, scanView.getScanViewPluginConfig(), "OCR");
        scanView.setScanViewPlugin(scanViewPlugin);
        scanViewPlugin.addScanResultListener(new ScanResultListener<OcrScanResult>() {
            @Override
            public void onResult(OcrScanResult ocrScanResult) {

                if(!ocrScanResult.toString().isEmpty()){
                    String result = ocrScanResult.getResult().toString().trim();
                        Constant.RESULT_SCAN_EKTP = result;
                     System.out.println("asd [ScanKtpFragment]"+result);
                    getActivity().onBackPressed();
                    scanView.stop();
                }
            }
        });
        scanViewPlugin.setDebugListener(this);
        txtclose = view.findViewById(R.id.txtclose);
        txtclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        scanView.start();
    }

    @Override
    public void onPause() {
        super.onPause();
        scanView.stop();
        scanView.releaseCameraInBackground();
    }


    @Override
    public void onDebug(String s, Object o) {
        if (AnylineDebugListener.BRIGHTNESS_VARIABLE_NAME.equals(s) &&
                (AnylineDebugListener.BRIGHTNESS_VARIABLE_CLASS.equals(o.getClass()) ||
                        AnylineDebugListener.BRIGHTNESS_VARIABLE_CLASS.isAssignableFrom(o.getClass()))) {
            switch (scanView.getBrightnessFeedBack()) {
                case TOO_BRIGHT:
                    //  Toast.makeText(getApplicationContext(), "Terlalu Terang", Toast.LENGTH_LONG).show();
                    break;
                case TOO_DARK:
                    // Toast.makeText(getApplicationContext(), "Terlalu Gelap", Toast.LENGTH_LONG).show();
                    break;
                case OK:
                    // Toast.makeText(getApplicationContext(), "Oke, Sesuai", Toast.LENGTH_LONG).show();
                    break;
            }
        } else if (AnylineDebugListener.DEVICE_SHAKE_WARNING_VARIABLE_NAME.equals(s)) {
            System.out.println("asd to shake");
        }
    }

    @Override
    public void onRunSkipped(RunFailure runFailure) {

    }


}
