package products.dak.com.smartbank.fragments.Product;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import products.dak.com.smartbank.R;
import products.dak.com.smartbank.interfaces.SnackBar;


public class RekeningGiroFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private SnackBar callback;


    public RekeningGiroFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static RekeningGiroFragment newInstance(String param1, String param2) {
        RekeningGiroFragment fragment = new RekeningGiroFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        callback.showSnackBar(true,"Rekening Giro");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_rekening_giro, container, false);
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try{
            callback = (SnackBar)context;
        }catch (Exception e){
            System.out.println("Das"+e.toString());
        }
    }



}
