package products.dak.com.smartbank.models;

public class Otp {
    private String rc;
    private String otp;
    private String rm;
    private String msisdn;

    public Otp(String rc, String otp, String rm, String msisdn) {
        this.rc = rc;
        this.otp = otp;
        this.rm = rm;
        this.msisdn = msisdn;
    }

    public String getRc() {
        return rc;
    }

    public String getOtp() {
        return otp;
    }

    public String getRm() {
        return rm;
    }

    public String getMsisdn() {
        return msisdn;
    }
}
