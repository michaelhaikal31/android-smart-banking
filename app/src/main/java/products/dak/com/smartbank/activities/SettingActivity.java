package products.dak.com.smartbank.activities;

import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import products.dak.com.smartbank.R;
import products.dak.com.smartbank.fragments.Setting.SettingMenuFragment;
import products.dak.com.smartbank.interfaces.AppBarControl;

public class SettingActivity extends AppCompatActivity implements AppBarControl {
    private ImageView logoKanan, logoKiri, btnClose, btnBack;
    private static final String ARG_PARAM1 = "menu";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private String mParam1, mParam2, mParam3;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }
        logoKanan = findViewById(R.id.logoAppBarRight);
        logoKiri = findViewById(R.id.logoAppBarLeft);
        btnBack = findViewById(R.id.btnBackAppBar);
        btnClose = findViewById(R.id.btnCloseAppBar);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mParam1 = bundle.getString(ARG_PARAM1);
            mParam2 = bundle.getString(ARG_PARAM2);
            mParam3 = bundle.getString(ARG_PARAM3);
        }
        if (mParam1.equalsIgnoreCase("toChangePin")) {

            SettingMenuFragment settingMenuFragment = new SettingMenuFragment();
            Bundle extras = new Bundle();
            extras.putString("param1", "formChangePassword");
            extras.putString("param2", mParam2);
            extras.putString("param3", "show");
            settingMenuFragment.setArguments(extras);
            addFragment(settingMenuFragment);

        }else {
            SettingMenuFragment settingMenuFragment = new SettingMenuFragment();
            addFragment(settingMenuFragment);
        }


    }

    private void addFragment(Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.frame_layout_setting, fragment, "menuSetting");
        ft.commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setBack() {
        logoKiri.setVisibility(View.INVISIBLE);
        btnBack.setVisibility(View.VISIBLE);
        btnClose.setVisibility(View.INVISIBLE);
        logoKanan.setVisibility(View.VISIBLE);
    }

    @Override
    public void setClose() {
        logoKiri.setVisibility(View.VISIBLE);
        btnBack.setVisibility(View.INVISIBLE);
        btnClose.setVisibility(View.VISIBLE);
        logoKanan.setVisibility(View.INVISIBLE);
    }
}
