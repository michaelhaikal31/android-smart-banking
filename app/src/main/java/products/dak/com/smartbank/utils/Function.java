package products.dak.com.smartbank.utils;

import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

import products.dak.com.smartbank.activities.dialog.DialogAlert;
import products.dak.com.smartbank.activities.dialog.DialogAlertHome;

public class Function {
    public static void showAlert(FragmentActivity mContext, String result) {
        Constant.DATA_ALERT_RM = result;
        DialogAlert callCenterDialog = new DialogAlert();
        callCenterDialog.show(mContext.getSupportFragmentManager()
                ,"Alert");
    }
    public static void showAlertHome(FragmentActivity mContext, String result){
        Constant.DATA_ALERT_RM = result;
        DialogAlertHome callCenterDialog=new DialogAlertHome();
        callCenterDialog.show(mContext.getSupportFragmentManager(), "Alert");
    }
}
