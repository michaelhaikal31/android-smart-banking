package products.dak.com.smartbank.fragments.Login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatDelegate;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import products.dak.com.smartbank.Sessions.UserSession;
import products.dak.com.smartbank.activities.main.MainActivity;
import products.dak.com.smartbank.activities.SettingActivity;
import products.dak.com.smartbank.animations.MoveAnimation;
import products.dak.com.smartbank.activities.main.FirstActivity;
import products.dak.com.smartbank.R;
import products.dak.com.smartbank.fragments.Otp.OtpVerificationFragment;
import products.dak.com.smartbank.interfaces.ApiService;
import products.dak.com.smartbank.interfaces.AppBarControl;
import products.dak.com.smartbank.models.Otp;
import products.dak.com.smartbank.models.Setting.Setting;
import products.dak.com.smartbank.models.User.User;
import products.dak.com.smartbank.models.User.ValueUser;
import products.dak.com.smartbank.services.RetrofitService;
import products.dak.com.smartbank.utils.Constant;
import products.dak.com.smartbank.utils.TripleDes;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;


public class Login extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "jenis";
    private static final String ARG_PARAM2 = "msisdn";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private PinEntryEditText otpEditText;
    private TextView text1, text2;
    private ProgressBar progressBar;
    private CountDownTimer countDownTimer;
    private TextView txtProgressBar, txt1, txt2, txt3;
    private Button btnLogin;
    EditText txtPhoneNumber;
    private JSONObject paramObject;
    private User user = new User();
    ProgressDialog progressDialog;
    private String encryptedBody;
    private TripleDes tripleDesPin, tripleDesJSon;
    private TextView kirimUlangOtp;
    private String fixNumber;//no hp yang sudah di perbaiki
    private AppBarControl appBarControl;

    public Login() {
        // Required empty public constructor
    }

    public static Login newInstance(String param1, String param2) {
        Login fragment = new Login();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = null;
        if (mParam1.equalsIgnoreCase("1")) {
            v = inflater.inflate(R.layout.fragment_login, container, false);
            ((FirstActivity) getActivity()).getSupportActionBar().show();
            ((FirstActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
            final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
            upArrow.setColorFilter(getResources().getColor(R.color.colorDarkBlue), PorterDuff.Mode.SRC_ATOP);
            ((FirstActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(upArrow);
            txtPhoneNumber = v.findViewById(R.id.txtPhoneNumber);
            btnLogin = v.findViewById(R.id.sign_in_button);

            btnLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (isEmpty(txtPhoneNumber) == false) {
                        progressDialog = new ProgressDialog(getActivity());
                        progressDialog.setMessage("Please wait");
                        progressDialog.show();
                        checkMsisdn();
                    }

                    /*

                     */
                }
            });
        } else if (mParam1.equalsIgnoreCase("changePhone") || mParam1.equalsIgnoreCase("changePassword")) {
            v = inflater.inflate(R.layout.fragment_otp_verification, container, false);
            //((SettingActivity) getActivity()).getSupportActionBar().show();
            appBarControl = (AppBarControl) getActivity();
            appBarControl.setClose();
            kirimUlangOtp = v.findViewById(R.id.txtKirimUlangOtp);
            kirimUlangOtp.setVisibility(View.INVISIBLE);
            //btnLogin = v.findViewById(R.id.sign_in_button2);
            btnLogin.setText("konfirmasi");
            kirimUlangOtp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    progressDialog = new ProgressDialog(getActivity());
                    progressDialog.setMessage("Please wait");
                    progressDialog.show();
                    resendOtp();
                }
            });
            btnLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /*
                    Intent i = new Intent(getActivity(), MainActivity.class);
                    i.setFlags(i.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(i);
                    */
                    if (!mParam1.equalsIgnoreCase("changePhone")) {
                        progressDialog = new ProgressDialog(getActivity());
                        progressDialog.setMessage("Please wait");
                        progressDialog.show();
                        conf_change_pin("asd");
                    } else {
                        progressDialog = new ProgressDialog(getActivity());
                        progressDialog.setMessage("Please wait");
                        progressDialog.show();
                        conf_change_phone();
                    }

                }
            });
            progressBar = v.findViewById(R.id.progressBar);
            otpEditText = v.findViewById(R.id.txt_pin_entry);
            text2 = v.findViewById(R.id.textView2);
            txt1 = v.findViewById(R.id.textView2);
            txt2 = v.findViewById(R.id.textView3);
            txt3 = v.findViewById(R.id.textView15);
            txt1.setVisibility(View.INVISIBLE);
            txt2.setVisibility(View.INVISIBLE);
            txt3.setText("Masukan OTP yang kami kirim melalui sms dibawah ini !");
            txtProgressBar = v.findViewById(R.id.txtProgressBar);
            startCountdown();


        } else if (mParam1.equalsIgnoreCase("transfer") || mParam1.equalsIgnoreCase("isi_pulsa") || mParam1.equalsIgnoreCase("post_paid") || mParam1.equalsIgnoreCase("pln")) {
            v = inflater.inflate(R.layout.fragment_otp_verification, container, false);
            appBarControl = (AppBarControl) getActivity();
            appBarControl.setClose();
            kirimUlangOtp = v.findViewById(R.id.txtKirimUlangOtp);
            kirimUlangOtp.setVisibility(View.INVISIBLE);
            //btnLogin = v.findViewById(R.id.sign_in_button2);
            btnLogin.setText("konfirmasi");
            btnLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /*
                    Intent i = new Intent(getActivity(), MainActivity.class);
                    i.setFlags(i.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(i);
                    */
                    if (mParam1.equalsIgnoreCase("transfer")) {
                        progressDialog = new ProgressDialog(getActivity());
                        progressDialog.setMessage("Please wait");
                        progressDialog.show();
                        otp_transfer();
                    }


                }
            });
            progressBar = v.findViewById(R.id.progressBar);
            otpEditText = v.findViewById(R.id.txt_pin_entry);
            text2 = v.findViewById(R.id.textView2);
            txt1 = v.findViewById(R.id.textView2);
            txt2 = v.findViewById(R.id.textView3);
            txt3 = v.findViewById(R.id.textView15);
            txt1.setVisibility(View.INVISIBLE);
            txt2.setVisibility(View.INVISIBLE);
            txt3.setText("Masukan OTP yang kami kirim melalui sms dibawah ini !");
            txtProgressBar = v.findViewById(R.id.txtProgressBar);
            startCountdown();
        }

        return v;
    }

    private void startCountdown() {
        countDownTimer = new CountDownTimer(3000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                int second = (int) ((millisUntilFinished / 1000));
                //txtProgressBar.setText(second);
                txtProgressBar.setText(String.valueOf(second));
                progressBar.setProgress(second);
            }

            @Override
            public void onFinish() {
                kirimUlangOtp.setVisibility(View.VISIBLE);
            }
        };

        countDownTimer.start();

    }


    //ini untuk animasi bergerak
    /*
    private void success(){
      final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
      imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
      text2.animate().alpha(0.0f);
      otpEditText.animate().alpha(0.0f);
      ((FirstActivity) getActivity()).animteTitle();

    }
    */

    //cek nomor hp apakah tersedia
    private void checkMsisdn() {
        final String key = Constant.getKey(24);
        ApiService apiInterface = RetrofitService.getRetrofit(key).create(ApiService.class);
        paramObject = new JSONObject();
        try {
            tripleDesJSon = new TripleDes(key);
            fixNumber = String.valueOf(txtPhoneNumber.getText().toString().charAt(0));
            if (fixNumber.equalsIgnoreCase("0")) {
                StringBuilder newPhone = new StringBuilder(txtPhoneNumber.getText());
                newPhone.deleteCharAt(0);
                fixNumber = "62" + newPhone;
            } else {
                fixNumber = txtPhoneNumber.getText().toString();
            }
            System.out.println("asd " + fixNumber);
            paramObject.put("msisdn", fixNumber);
            encryptedBody = tripleDesJSon.encrypt(paramObject.toString());
        } catch (JSONException e) {
            System.out.println("asd JSON Exception : " + e.toString());
        } catch (Exception e) {
            System.out.println("asd encrypt error :" + e.toString());
        }
        System.out.println("asd enc " + encryptedBody);
        Call<String> call = apiInterface.check_msisdn(encryptedBody);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try {
                    //System.out.println("asd respon "+tripleDes.decrypt(response.toString()));
                    ValueUser valueUser = new Gson().fromJson(tripleDesJSon.decrypt(response.body()), ValueUser.class);
                    if (valueUser.getRc().equalsIgnoreCase("00")) {
                        //jika response field flag = "1" goto Change password
                        if (valueUser.getFlag_reset_password().equalsIgnoreCase("1")) {
                            Intent intent = new Intent(getActivity(), SettingActivity.class);
                            intent.putExtra("menu", "toChangePin");
                            intent.putExtra("param2", valueUser.getMsisdn());
                            startActivity(intent);
                        } else {
                            //jika response field flag = "0" goto Home Page
                            if (valueUser.getFlag_reset_password().equalsIgnoreCase("0")) {
                                user = valueUser.getData();
                                final UserSession session = new UserSession();
                                session.setName(getActivity(), valueUser.getData().name);
                                session.setMsiSdn(getActivity(), valueUser.getMsisdn());
                                Intent intent = new Intent(getActivity(), MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                getActivity().finish();
                            } else {
                                user = valueUser.getData();
                                final UserSession session = new UserSession();
                                session.setName(getActivity(), valueUser.getData().name);
                                OtpVerificationFragment otpVerificationFragment = new OtpVerificationFragment();
                                Bundle extras = new Bundle();
                                extras.putInt("code", 1);
                                extras.putString("msisdn", fixNumber);
                                otpVerificationFragment.setArguments(extras);
                                FragmentManager fm = getActivity().getSupportFragmentManager();
                                FragmentTransaction ft = fm.beginTransaction();
                                ft.replace(R.id.frame_layout1, otpVerificationFragment);
                                ft.addToBackStack(null);
                                ft.commit();
                            }
                        }

                    } else {
                        Snackbar.make(getView(), valueUser.getRm(), Snackbar.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    System.out.println("asd catch " + e.toString());
                    Snackbar.make(getView(), "Cannot processing server's response !", Snackbar.LENGTH_SHORT).show();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Snackbar.make(getView(), "Oops, Something wrong please try again later or check your internet connection", Snackbar.LENGTH_SHORT).show();
                System.out.println("asd failure " + t.toString());
                progressDialog.dismiss();
            }
        });

    }

    private void conf_change_pin(String method) {
        final String key = Constant.getKey(24);
        ApiService apiInterface = RetrofitService.getRetrofit(key).create(ApiService.class);
        paramObject = new JSONObject();
        final UserSession userSession = new UserSession();
        try {
            tripleDesPin = new TripleDes(Constant.KEY_DES_PIN);
            tripleDesJSon = new TripleDes(key);
            paramObject.put("msisdn", userSession.getMsiSdn(getActivity()));
            paramObject.put("otp", tripleDesPin.encrypt(otpEditText.getText().toString()));
            encryptedBody = tripleDesJSon.encrypt(paramObject.toString());
        } catch (JSONException e) {
        } catch (Exception e) {
        }

        System.out.println("asd " + paramObject.toString());

        Call<String> call = apiInterface.conf_change_pin(encryptedBody);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try {
                    //Toast.makeText(getActivity(),response.body().toString(),Toast.LENGTH_SHORT).show();;
                    Setting.changePin changePin = new Gson().fromJson(tripleDesJSon.decrypt(response.body()), Setting.changePin.class);
                    if (changePin.rm.equalsIgnoreCase("Success")) {
                        userSession.setEncryptedBody(getActivity(), "");
                        userSession.setTripleDesKey(getActivity(), "");
                        Toast.makeText(getActivity(), changePin.rm, Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(getActivity(), FirstActivity.class);
                        startActivity(i);
                        getActivity().finish();

                    } else {
                        Snackbar.make(getView(), changePin.rm, Snackbar.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    System.out.println("asd catch " + e.toString());
                    Snackbar.make(getView(), "Cannot processing server's response !", Snackbar.LENGTH_SHORT).show();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Snackbar.make(getView(), "Oops, Something wrong please try again later", Snackbar.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    private void conf_change_phone() {
        final String key = Constant.getKey(24);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                Request request = original.newBuilder()
                        .header("x-api-key", key)
                        .header("User-Agent", Constant.PARTNER_IDENTIFIER)
                        .header("Authorization", Constant.PARTNER_AUTH)
                        .method(original.method(), original.body())
                        .build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE_SECURE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);
        paramObject = new JSONObject();
        final UserSession userSession = new UserSession();
        try {
            tripleDesPin = new TripleDes(Constant.KEY_DES_PIN);
            tripleDesJSon = new TripleDes(key);
            paramObject.put("msisdn", userSession.getMsiSdn(getActivity()));
            paramObject.put("otp", tripleDesPin.encrypt(otpEditText.getText().toString()));
            encryptedBody = tripleDesJSon.encrypt(paramObject.toString());
        } catch (JSONException e) {
        } catch (Exception e) {

        }
        System.out.println("asd " + paramObject.toString());
        Call<String> call = apiInterface.conf_change_msisdn(encryptedBody);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try {
                    //Toast.makeText(getActivity(),response.body().toString(),Toast.LENGTH_SHORT).show();;
                    Setting.changeMsisdn changeMsisdn = new Gson().fromJson(tripleDesJSon.decrypt(response.body()), Setting.changeMsisdn.class);
                    if (changeMsisdn.rm.equalsIgnoreCase("Success")) {
                        Toast.makeText(getActivity(), changeMsisdn.rm, Toast.LENGTH_SHORT).show();
                        userSession.setTripleDesKey(getActivity(), "");
                        userSession.setEncryptedBody(getActivity(), "");
                        Intent i = new Intent(getActivity(), FirstActivity.class);
                        startActivity(i);
                        getActivity().finish();
                    } else {
                        Snackbar.make(getView(), changeMsisdn.rm, Snackbar.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    System.out.println("asd catch " + e.toString());
                    Snackbar.make(getView(), "Cannot processing server's response !", Snackbar.LENGTH_SHORT).show();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Snackbar.make(getView(), "Oops, Something wrong please try again later", Snackbar.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    private void otp_transfer() {
        final String key = Constant.getKey(24);
        ApiService apiInterface = RetrofitService.getRetrofit(key).create(ApiService.class);
        paramObject = new JSONObject();
        final UserSession userSession = new UserSession();
        try {
            tripleDesJSon = new TripleDes(key);
            tripleDesPin = new TripleDes(Constant.KEY_DES_PIN);
            paramObject.put("msisdn", userSession.getMsiSdn(getActivity()));
            paramObject.put("otp", tripleDesPin.encrypt(otpEditText.getText().toString()));
            encryptedBody = tripleDesJSon.encrypt(paramObject.toString());
        } catch (JSONException e) {
            System.out.println("asd JSON Exception : " + e.toString());
        } catch (Exception e) {
            System.out.println("asd encrypt error :" + e.toString());
        }

        System.out.println("asd enc " + paramObject);
        Call<String> call = apiInterface.transfer_otp(encryptedBody);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try {
                    //System.out.println("asd respon "+tripleDes.decrypt(response.toString()));
                    Otp otp = new Gson().fromJson(tripleDesJSon.decrypt(response.body()), Otp.class);
                    if (otp.getRc().equalsIgnoreCase("Success")) {
                        userSession.setTripleDesKey(getActivity(), "");
                        userSession.setEncryptedBody(getActivity(), "");
                        Snackbar.make(getView(), "Successfully", Snackbar.LENGTH_SHORT).show();
                        getActivity().finish();
                    } else {
                        Snackbar.make(getView(), otp.getRm(), Snackbar.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    System.out.println("asd catch " + e.toString());
                    Snackbar.make(getView(), "Cannot processing server's response !", Snackbar.LENGTH_SHORT).show();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Snackbar.make(getView(), "Oops, Something wrong please try again later or check your internet connection", Snackbar.LENGTH_SHORT).show();
                System.out.println("asd failure " + t.toString());
                progressDialog.dismiss();
            }
        });


    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (mParam1.equalsIgnoreCase("1")) {
            return MoveAnimation.create(MoveAnimation.RIGHT, enter, 300);
        } else {
            return MoveAnimation.create(MoveAnimation.LEFT, enter, 300);
        }
    }

    private boolean isEmpty(EditText etText) {

        if (etText.getText().toString().trim().length() == 0) {
            etText.setError("Cannot empty");
            System.out.println("asd asd");
            return true;
        } else {
            return false;
        }

    }

    private void resendOtp() {
        final UserSession userSession = new UserSession();
        try {
            tripleDesJSon = new TripleDes(userSession.getTripleDesKey(getActivity()));
        } catch (Exception e) {
        }
        ApiService apiInterface = RetrofitService.getRetrofit(userSession.getTripleDesKey(getActivity())).create(ApiService.class);
        System.out.println("asd resend " + userSession.getEncryptedBody(getActivity()));
        Call<String> call = null;


        //mengcek dari parameter apakah jenis proses otp yang dibutuhkan
        if (mParam1.equalsIgnoreCase("changePhone")) {
            call = apiInterface.change_msisdn(userSession.getEncryptedBody(getActivity()));
        } else if (mParam1.equalsIgnoreCase("changePassword")) {
            call = apiInterface.change_pin(userSession.getEncryptedBody(getActivity()));
        } else if (mParam1.equalsIgnoreCase("transfer")) {
            call = apiInterface.conf_transfer(userSession.getEncryptedBody(getActivity()));
        }
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try {
                    //Toast.makeText(getActivity(),response.body().toString(),Toast.LENGTH_SHORT).show();;
                    Setting.changeMsisdn changeMsisdn = new Gson().fromJson(tripleDesJSon.decrypt(response.body()), Setting.changeMsisdn.class);
                    if (changeMsisdn.rm.equalsIgnoreCase("Success")) {
                        Snackbar.make(getView(), "We have sent icon_change_pin new code via SMS", Snackbar.LENGTH_SHORT).show();
                    } else {
                        Snackbar.make(getView(), changeMsisdn.rm, Snackbar.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    System.out.println("asd catch " + e.toString());
                    Snackbar.make(getView(), "Cannot processing server's response !", Snackbar.LENGTH_SHORT).show();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                System.out.println("asd failure " + t.toString() + " | " + t.getMessage());
                Snackbar.make(getView(), "Oops, Something wrong please try again later", Snackbar.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }


    @Override
    public void onResume() {
        System.out.println("asd param " + mParam1);
        super.onResume();
    }

    @Override
    public void onPause() {
        System.out.println("asd param " + mParam1);
        super.onPause();
    }


}
