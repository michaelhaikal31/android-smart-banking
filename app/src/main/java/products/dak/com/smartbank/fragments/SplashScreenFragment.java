package products.dak.com.smartbank.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;

import products.dak.com.smartbank.R;
import products.dak.com.smartbank.Sessions.UserSession;
import products.dak.com.smartbank.activities.main.FirstActivity;
import products.dak.com.smartbank.activities.main.MainActivity;
import products.dak.com.smartbank.animations.MoveAnimation;
import products.dak.com.smartbank.fragments.Login.Login;
import products.dak.com.smartbank.fragments.Register.Register2Fragment;
import products.dak.com.smartbank.interfaces.FragmentChangeListener;


public class SplashScreenFragment extends Fragment implements FragmentChangeListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "step";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private Button btnPunyaAkun,btnBlmPunyaAkun;


    public SplashScreenFragment() {
        // Required empty public constructor
    }

    public static SplashScreenFragment newInstance(String param1, String param2) {
        SplashScreenFragment fragment = new SplashScreenFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = null;
        if(mParam1.equalsIgnoreCase("2")) {
            v = inflater.inflate(R.layout.fragment_splashscreen_tanya_akun,container,false);
            ((FirstActivity) getActivity()).getSupportActionBar().hide();
            btnBlmPunyaAkun = v.findViewById(R.id.btnBlmPunyaAkun);
            btnPunyaAkun = v.findViewById(R.id.btnPunyaAkun);
            btnBlmPunyaAkun.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    RegisterFragment registerFragment = new RegisterFragment();
//                    Bundle extras  = new Bundle();
//                    extras.putString("step","1");
//                    registerFragment.setArguments(extras);
//                    FragmentManager fm = getActivity().getSupportFragmentManager();
//                    FragmentTransaction ft = fm.beginTransaction();
//                    ft.replace(R.id.frame_layout1,registerFragment);
//                    ft.addToBackStack(null);
//                    ft.commit();
                    Register2Fragment register2Fragment = new Register2Fragment();
                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    FragmentTransaction ft = fm.beginTransaction();
                    ft.replace(R.id.frame_layout1,register2Fragment);
                    ft.addToBackStack(null);
                    ft.commit();
                }
            });

            btnPunyaAkun.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Login login = new Login();
                    Bundle extras  = new Bundle();
                    extras.putString("jenis","1");
                    login.setArguments(extras);
                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    FragmentTransaction ft = fm.beginTransaction();
                    ft.replace(R.id.frame_layout1,login);
                    ft.addToBackStack(null);
                    ft.commit();
                }
            });

            return v;
        }else{
            v = inflater.inflate(R.layout.fragment_splash_screen, container, false);
            final Handler handler = new Handler();
            ((FirstActivity) getActivity()).getSupportActionBar().hide();
            handler.postDelayed(new Runnable() {

                @Override

                public void run() {
                    UserSession userSession = new UserSession();
                    if(userSession.getMsiSdn(getActivity()).equalsIgnoreCase("")){
                        SplashScreenFragment splashScreenFragment = new SplashScreenFragment();
                        Bundle extras = new Bundle();
                        extras.putString("step", "2");
                        splashScreenFragment.setArguments(extras);
                        FragmentManager fm = getActivity().getSupportFragmentManager();
                        FragmentTransaction ft = fm.beginTransaction();
                        ft.replace(R.id.frame_layout1, splashScreenFragment);
                        ft.commit();
                    }else{
                        Intent i = new Intent(getActivity(), MainActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                        getActivity().finish();
                    }

                }

            }, 1000L); //3000 L = 3 detik
            return v;
        }
    }

    /*

       Login login = new Login();
                Bundle extras  = new Bundle();
                extras.putString("jenis","1");
                login.setArguments(extras);
                FragmentManager fm = getActivity().getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.frame_layout1,login);
                ft.commit();
     */
    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if(mParam1.equalsIgnoreCase("1")){
            return MoveAnimation.create(MoveAnimation.UP, enter, 300);
        }else{
            return MoveAnimation.create(MoveAnimation.LEFT, enter, 300);
        }
    }

    @Override
    public void replaceFragment(Fragment fragment) {

    }

    @Override
    public boolean onFragmentBackPressed() {
        getActivity().finish();
        return true;
    }
}
