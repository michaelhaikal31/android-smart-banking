package products.dak.com.smartbank.models.Mutasi;


import java.util.List;

public class ValueMutasi {
    public String rc;
    public List<Mutasi> data;
    public String rm;
    public String msisdn;


    public String getRc() {
        return rc;
    }

    public List<Mutasi> getData() {
        return data;
    }

    public String getRm() {
        return rm;
    }

    public String getMsisdn() {
        return msisdn;
    }
}
