package products.dak.com.smartbank.interfaces;

public interface SnackBar {
    void showSnackBar(boolean show, String name);
}
