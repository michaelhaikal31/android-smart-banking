package products.dak.com.smartbank.fragments.Pembelian;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import products.dak.com.smartbank.R;
import products.dak.com.smartbank.Sessions.UserSession;
import products.dak.com.smartbank.interfaces.ApiService;
import products.dak.com.smartbank.services.RetrofitService;
import products.dak.com.smartbank.utils.Constant;
import products.dak.com.smartbank.utils.ConstantTransaction;
import products.dak.com.smartbank.utils.TripleDes;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PulsaFragment extends Fragment {
    private final static int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 1122;
    public static ArrayList<String> subpembayaran = new ArrayList<String>();

    /*PARAMETER*/
    private static final String ARG_PARAM1 = "step";
    private static final String ARG_PARAM2 = "no_handphone";
    private static final String ARG_PARAM3 = "provider";
    private static final String ARG_PARAM4 = "jumlah_tagihan";
    private String mParam1;
    private String mParam2;
    private String mParam3;
    private String mParam4;

    /*VARIABEL*/
    private String rcResult, rmResult;
    private boolean verified = false;
    private Button btnBeli;
    private ImageView btnContactList;
    private EditText txtNoHandphone, txtPin, txtPinIsiPulsa;
    private Spinner txtNominal;
    private TextInputLayout textInputLayout1, textInputLayout2;
    private TextView keterangan;
    private TripleDes tripleDesJSon, tripleDesBody, tripleDesPin;
    private ApiService apiService;
    private String fixNumber;
    private String encryptedBody;
    private TextView tvCekDenom, otplayout, labelspNominal;
    private String[] arrayDenoma;
    private BottomSheetDialog mBottomSheetDialog;
    private ProgressDialog progressDialog;
    private LinearLayout layout223;


    final UserSession userSession = new UserSession();

    public PulsaFragment() {
        // Required empty public constructor
    }

    public static PulsaFragment newInstance(String param1, String param2, String param3, String param4) {
        PulsaFragment fragment = new PulsaFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        args.putString(ARG_PARAM3, param3);
        args.putString(ARG_PARAM4, param4);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            mParam3 = getArguments().getString(ARG_PARAM3);
            mParam4 = getArguments().getString(ARG_PARAM4);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = null;

        //Layout Default
        v = inflater.inflate(R.layout.fragment_isi_pulsa_step_one, container, false);
        btnBeli = v.findViewById(R.id.btnBeliPulsa1);
        btnContactList = v.findViewById(R.id.btnContactList);
        txtNoHandphone = v.findViewById(R.id.txtNoHandphone);
        txtNominal = v.findViewById(R.id.spNominal);
        textInputLayout2 = v.findViewById(R.id.textInputLayout2);
        labelspNominal = v.findViewById(R.id.labelspNominal);
        labelspNominal.setVisibility(View.INVISIBLE);
        //Add Contact
        btnContactList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                    // Permission is not granted
                    // Should we show an explanation?
                    if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_CONTACTS)) {
                        // Show an explanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle("Permission Request");
                        alert.setMessage("Dengan mengizinkan kami mengakses kontak anda akan memudahkan kami untuk memberikan fitur pencarian nomor telepon yang lebih baik");
                        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(getActivity(),
                                        new String[]{Manifest.permission.READ_CONTACTS},
                                        MY_PERMISSIONS_REQUEST_READ_CONTACTS);
                            }
                        });
                        alert.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                return;
                            }
                        });
                        alert.create();
                        alert.show();
                    } else {
                        // No explanation needed; request the permission
                        ActivityCompat.requestPermissions(getActivity(),
                                new String[]{Manifest.permission.READ_CONTACTS},
                                MY_PERMISSIONS_REQUEST_READ_CONTACTS);
                    }
                } else {
                    // Permission has already been granted
                    Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                    startActivityForResult(intent, 1);
                }

            }
        });
        txtNoHandphone.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    check_phone_number();
                }
                return false;
            }
        });
        txtNoHandphone.onEditorAction(EditorInfo.IME_ACTION_DONE);
        btnBeli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtNoHandphone.getText().length() >= 11 && txtNoHandphone.getText().length() <= 13) {
                    if (verified) {
                        KonfirmasiPembelianFragment konfirmasi = new KonfirmasiPembelianFragment();
//                            Bundle extras = new Bundle();
//                            extras.putString(ConstantTransaction.MSISDN_PEMBELIAN_PULSA, fixNumber);
//                            extras.putString("no_rek", txtNoHandphone.getText().toString());
//                            isiPulsaFragment.setArguments(extras);
                        FragmentManager fm = getActivity().getSupportFragmentManager();
                        FragmentTransaction ft = fm.beginTransaction();
                        ft.replace(R.id.frame_layout_super, konfirmasi);
                        ft.commit();
                        verified = false;
                    } else {
                        check_phone_number();
                    }
                } else {
                    txtNoHandphone.setError("harap isi nomor telepon tujuan");
                }
            }
        });

        return v;
    }

    public void callDialogShow() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait");
        progressDialog.show();
    }


    private void check_phone_number() {
        callDialogShow();
        final String key = Constant.getKey(24);
        apiService = RetrofitService.getRetrofitDev(key).create(ApiService.class);
        JSONObject paramObject = new JSONObject();

        try {
            fixNumber = String.valueOf(txtNoHandphone.getText().toString().charAt(0));
            if (fixNumber.equalsIgnoreCase("0")) {
                StringBuilder newPhone = new StringBuilder(txtNoHandphone.getText());
                newPhone.deleteCharAt(0);
                fixNumber = "62" + newPhone;
            } else {
                fixNumber = txtNoHandphone.getText().toString();
            }
            paramObject.put(ConstantTransaction.MY_MSIDN, userSession.getMsiSdn(getActivity()));
            paramObject.put(ConstantTransaction.MY_DESTINATION, fixNumber);
        } catch (JSONException e) {
            System.out.println("asd JSON Exception : " + e.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        Call<String> call = apiService.send_pulsa(paramObject.toString());
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(final Call<String> call, Response<String> response) {
                sout(response.body());
                try {
                    JSONObject obj = new JSONObject(response.body());
                    JSONObject object = obj.getJSONObject(ConstantTransaction.DATA);
                    JSONArray jArray = object.getJSONArray(ConstantTransaction.MY_DENOM);

                    arrayDenoma = new String[jArray.length()];
                    for (int i = 0; i < jArray.length(); i++) {
                        arrayDenoma[i] = jArray.getString(i);
                    }
                    ArrayAdapter<String> adapterdenoma = new ArrayAdapter<String>(getActivity(),
                            android.R.layout.simple_dropdown_item_1line, arrayDenoma);

                    txtNominal.setAdapter(adapterdenoma);
                    txtNominal.setClipToPadding(true);
                    txtNominal.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            getinquiry(arrayDenoma[position], userSession);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });

                    tvCekDenom.setVisibility(View.INVISIBLE);
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Snackbar.make(getView(), "Oops, Something wrong please try again later or check your internet connection", Snackbar.LENGTH_SHORT).show();
                System.out.println("asd failure " + t.toString());
                progressDialog.dismiss();
            }
        });
        labelspNominal.setVisibility(View.VISIBLE);
        txtNominal.setVisibility(View.VISIBLE);
    }

    private void getinquiry(String position, UserSession userSession) {
        try {
            JSONObject paramObject = new JSONObject();
            paramObject.put(ConstantTransaction.MY_MSIDN, userSession.getMsiSdn(getActivity()));
            paramObject.put(ConstantTransaction.MY_DESTINATION, fixNumber);
            paramObject.put(ConstantTransaction.MY_DENOM, position);

            Call<String> call = apiService.hargajual_pulsa(paramObject.toString());
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    sout(response.body());
                    try {
                        JSONObject obj = new JSONObject(response.body());
                        //JSONObject object = obj.getJSONObject("data");
                        rcResult = obj.getString(ConstantTransaction.RC_STATUS);
                        if (rcResult.equalsIgnoreCase(ConstantTransaction.RC_SUCCESS)) {
                            String data = obj.getString(ConstantTransaction.DATA);
                            Constant.DATA_RESULT = new JSONArray(data);

                        } else {
                            Toas("Sistem Error");
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    sout(t.toString());
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
        verified = true;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            Uri contactData = data.getData();
            Cursor c = getActivity().getContentResolver().query(contactData, null, null, null, null);
            if (c.moveToFirst()) {
                try {
                    String phoneNumber = "", emailAddress = "";
                    String name = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                    String contactId = c.getString(c.getColumnIndex(ContactsContract.Contacts._ID));
                    //http://stackoverflow.com/questions/866769/how-to-call-android-contacts-list   our upvoted answer

                    String hasPhone = c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

                    if (hasPhone.equalsIgnoreCase("1"))
                        hasPhone = "true";
                    else
                        hasPhone = "false";

                    if (Boolean.parseBoolean(hasPhone)) {
                        Cursor phones = getActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId, null, null);
                        while (phones.moveToNext()) {
                            phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        }
                        phones.close();
                    }

                    // Find Email Addresses
                    Cursor emails = getActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = " + contactId, null, null);
                    while (emails.moveToNext()) {
                        emailAddress = emails.getString(emails.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                    }
                    emails.close();

                    //mainActivity.onBackPressed();
                    // Toast.makeText(mainactivity, "go go go", Toast.LENGTH_SHORT).show();


                    //tvphone.setText("Phone: "+phoneNumber);
                    //tvmail.setText("Email: "+emailAddress);
                    txtNoHandphone.setText(phoneNumber.replace("-", ""));
                    check_phone_number();
                    Log.d("curs", name + " num" + phoneNumber + " " + "mail" + emailAddress);
                } catch (Exception e) {
                    Toas(e.toString());
                }

            }
            c.close();
        }
    }

    private void Toas(String s) {
        Toast.makeText(getContext(), s, Toast.LENGTH_SHORT).show();
    }

    private void sout(String s) {
        System.out.println("asd " + s);
    }
}
