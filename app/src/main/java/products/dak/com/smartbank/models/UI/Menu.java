package products.dak.com.smartbank.models.UI;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mikepenz.fastadapter.items.AbstractItem;

import products.dak.com.smartbank.R;

public class Menu extends AbstractItem<Menu,Menu.ViewHolder> {
    private String namaMenu;
    private String iconMenu;

    public Menu() {
    }

    public Menu(String namaMenu, String iconMenu) {
        this.namaMenu = namaMenu;
        this.iconMenu = iconMenu;
    }

    public String getNamaMenu() {
        return namaMenu;
    }

    public void setNamaMenu(String namaMenu) {
        this.namaMenu = namaMenu;
    }

    public String getIconMenu() {
        return iconMenu;
    }

    public void setIconMenu(String iconMenu) {
        this.iconMenu = iconMenu;
    }

    @Override
    public int getType() {
        return R.id.item_1;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.row_menu;
    }

    @Override
    public void bindView(Menu.ViewHolder holder) {
        super.bindView(holder);
        holder.namaMenu.setText(namaMenu);
        //holder.iconMenu.setImageResource();
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder{
        TextView namaMenu;
        ImageView iconMenu;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            namaMenu = itemView.findViewById(R.id.namaMenu);
            iconMenu = itemView.findViewById(R.id.iconMenu);
        }
    }
}
