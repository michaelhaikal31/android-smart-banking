package products.dak.com.smartbank.interfaces;


/*
interface ini dibuat agar fragment dapat
mengatur tombol serta logo pada app bar ketika sedang menggunakan super activity
 */
public interface AppBarControl {
    void setBack();//tampilkan app bar dengan backButton
    void setClose();//tampilkan app bar dengan closeButton
}
