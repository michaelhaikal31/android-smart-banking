package products.dak.com.smartbank.fragments.Perbankan;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import products.dak.com.smartbank.R;
import products.dak.com.smartbank.Sessions.UserSession;
import products.dak.com.smartbank.interfaces.ApiService;
import products.dak.com.smartbank.interfaces.AppBarControl;
import products.dak.com.smartbank.models.Mutasi.Mutasi;
import products.dak.com.smartbank.models.Mutasi.ValueDetailMutasi;
import products.dak.com.smartbank.services.RetrofitService;
import products.dak.com.smartbank.utils.Constant;
import products.dak.com.smartbank.utils.TripleDes;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DetailFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "type";
    private static final String ARG_PARAM2 = "mParam2";

    // TODO: Rename and change types of parameters
    private int mParam1;
    private String mParam2;
    private JSONObject paramObject;
    private TripleDes tripleDesJSon;
    private String encryptedBody;
    private List<Mutasi> listMutasi;
    private ProgressDialog progressDialog;
    private TextView txtData1,txtData2,txtData3,txtData4,txtData5,txtData6,txtData7,txtData8;
    private TextView txtTitle1,txtTitle2,txtTitle3,txtTitle4,txtTitle5,txtTitle6,txtTitle7,txtTitle8,txtTitle9;
    private Button okBtn;
    private AppBarControl appBarControl;


    public DetailFragment() {
        // Required empty public constructor
    }

    public static DetailFragment newInstance(int param1, String param2) {
        DetailFragment fragment = new DetailFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getInt(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_detail_mutasi, container, false);
        appBarControl = (AppBarControl)getActivity();

        txtData1 = v.findViewById(R.id.txtAmountDetailMutasi);
        txtData2 = v.findViewById(R.id.txtDetailMutasi);
        txtData3 = v.findViewById(R.id.txtDateDetailMutasi);
        txtData4 = v.findViewById(R.id.txtDestinationDetailMutasi);
        txtData5 = v.findViewById(R.id.txtTypetrxDetail);
        txtData6 = v.findViewById(R.id.txtTrxDetail);
        txtData7 = v.findViewById(R.id.txtTrxReffDetail);
        txtData8 = v.findViewById(R.id.txtStatusDetail);
        txtTitle1 = v.findViewById(R.id.txtTitleAmount);
        txtTitle2 = v.findViewById(R.id.txtTitleDetail);
        txtTitle3 = v.findViewById(R.id.txtDateTitle);
        txtTitle4 = v.findViewById(R.id.txtTitleDestination);
        txtTitle5 = v.findViewById(R.id.txtTitleTypeTrxDetail);
        txtTitle6 = v.findViewById(R.id.txtTitleTrxDetail);
        txtTitle7 = v.findViewById(R.id.txtTitleTrxReffDetail);
        txtTitle8 = v.findViewById(R.id.txtTitleStatusDetail);
        okBtn = v.findViewById(R.id.btnOkDetailMutasi);
        txtTitle9 = v.findViewById(R.id.textView14);
        switch (mParam1){
            case 1:
                txtTitle9.setText("Detail Mutasi");
                break;
            case 2:
                txtTitle9.setText("Detail Transfer");
                break;
        }



        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        switch (mParam1){
            case 1:
                appBarControl.setBack();
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Please wait");
                progressDialog.show();
                getDetailMutasi();
                break;
            case 2:
                appBarControl.setClose();
                detail_transfer();
                break;

        }


        return v;
    }


    private void getDetailMutasi(){
        final String key = Constant.getKey(24);
        ApiService apiInterface = RetrofitService.getRetrofit(key).create(ApiService.class);
        paramObject = new JSONObject();
        final UserSession userSession = new UserSession();
        try{
            tripleDesJSon = new TripleDes(key);
            paramObject.put("msisdn",userSession.getMsiSdn(getActivity()));
            paramObject.put("trx_reff",mParam2);
            encryptedBody = tripleDesJSon.encrypt(paramObject.toString());
        }catch (JSONException e){
        }catch (Exception e){

        }
        System.out.println("asd "+paramObject);
        Call<String> call = apiInterface.get_DetailMutasi(encryptedBody);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try{
                    //Toast.makeText(getActivity(),response.body().toString(),Toast.LENGTH_SHORT).show();;

                    ValueDetailMutasi detailMutasi = new Gson().fromJson(tripleDesJSon.decrypt(response.body()),ValueDetailMutasi.class);
                    if(detailMutasi.rm.equalsIgnoreCase("Success")){
                        txtData1.setText(detailMutasi.getData().getAmount());
                        txtData2.setText(detailMutasi.getData().getTrx_dtl());
                        txtData3.setText(detailMutasi.getData().getDate_time_trx());
                        txtData4.setText(detailMutasi.getData().getDestination());
                        txtData5.setText(detailMutasi.getData().getTrx_type());
                        txtData6.setText(detailMutasi.getData().getTrx());
                        txtData7.setText(detailMutasi.getData().getTrx_reff());
                        txtData8.setText(detailMutasi.getData().getStatus());

                    }else{
                        Snackbar.make(getView(),detailMutasi.rm,Snackbar.LENGTH_SHORT).show();
                    }

                }catch(Exception e){
                    System.out.println("asd catch "+e.toString());
                    Snackbar.make(getView(),"Cannot processing server's response detail!",Snackbar.LENGTH_SHORT).show();
                }
                progressDialog.dismiss();
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Snackbar.make(getView(),"Oops, Something wrong please try again later",Snackbar.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    private void detail_transfer(){
        try {
            JSONObject jsonData = new JSONObject(mParam2).getJSONObject("data");
            txtData1.setText(jsonData.getString("amount"));
            txtData2.setText("Transfer");
            txtData3.setText(jsonData.getString("date_time_trx"));
            txtTitle4.setText("Destination Name");
            txtData4.setText(jsonData.getString("destination_name"));
            txtTitle5.setText("Destination");
            txtData5.setText(jsonData.getString("destination_account"));
            txtTitle6.setText("Transfer Code");
            txtData6.setText(jsonData.getString("reff_number"));
            txtTitle7.setText("Status");
            txtData7.setText("Success");
            txtTitle8.setVisibility(View.INVISIBLE);
            txtData8.setVisibility(View.INVISIBLE);

        }catch (Exception e){
            Toast.makeText(getActivity(), "Error when translating server's response detail", Toast.LENGTH_SHORT).show();
        }
    }

}
