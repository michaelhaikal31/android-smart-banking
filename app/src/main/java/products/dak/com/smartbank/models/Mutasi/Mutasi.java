package products.dak.com.smartbank.models.Mutasi;

import android.graphics.Color;
import android.support.annotation.ColorRes;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mikepenz.fastadapter.items.AbstractItem;

import products.dak.com.smartbank.R;

public class Mutasi extends AbstractItem<Mutasi,Mutasi.ViewHolder> {
    private String amount;
    private String date_time_trx;
    private String trx_type;
    private String trx;
    private String trx_reff;


    public Mutasi(String amount, String date_time_trx, String trx_type, String trx, String trx_reff) {
        this.amount = amount;
        this.date_time_trx = date_time_trx;
        this.trx_type = trx_type;
        this.trx = trx;
        this.trx_reff = trx_reff;
    }

    public String getAmount() {
        return amount;
    }

    public String getDate_time_trx() {
        return date_time_trx;
    }

    public String getTrx_type() {
        return trx_type;
    }

    public String getTrx() {
        return trx;
    }

    public String getTrx_reff() {
        return trx_reff;
    }

    @Override
    public int getType() {
        return R.id.item_1;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.row_mutasi;
    }



    @Override
    public void bindView(Mutasi.ViewHolder holder) {
        super.bindView(holder);
        holder.tanggal.setText(date_time_trx);
        holder.transaksi.setText(trx);
        holder.biaya.setText(amount);
        if(trx_type.equalsIgnoreCase("K")){
            holder.jenis.setImageResource(R.drawable.icon_left_green);
        }else{
            holder.jenis.setImageResource(R.drawable.icon_right_green);
        }
        if(holder.getAdapterPosition()%2==1){
            holder.row.setBackgroundColor(Color.parseColor("#eaeae8"));
            row=10;
        }else{
            row=11;
        }

    }

    protected static class ViewHolder extends RecyclerView.ViewHolder{
        TextView tanggal,transaksi,biaya;
        ImageView jenis;
        ConstraintLayout row;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tanggal = itemView.findViewById(R.id.txtTanggal);
            transaksi = itemView.findViewById(R.id.txtTransaksi);
            biaya = itemView.findViewById(R.id.txtBiaya);
            jenis = itemView.findViewById(R.id.imgJenisMutasi);
            row = itemView.findViewById(R.id.backgroundRow);
        }
    }
    private  int row=10;

    @Override
    public String toString() {
        return "Mutasi{" +
                "amount='" + amount + '\'' +
                ", date_time_trx='" + date_time_trx + '\'' +
                ", trx_type='" + trx_type + '\'' +
                ", trx='" + trx + '\'' +
                ", trx_reff='" + trx_reff + '\'' +
                '}';
    }
}
