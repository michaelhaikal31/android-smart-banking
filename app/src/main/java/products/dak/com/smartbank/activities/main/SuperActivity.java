package products.dak.com.smartbank.activities.main;

import android.graphics.Color;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import products.dak.com.smartbank.R;
import products.dak.com.smartbank.fragments.Pembelian.PulsaFragment;
import products.dak.com.smartbank.fragments.Perbankan.MutasiFragment;
import products.dak.com.smartbank.fragments.Pembelian.PostPaidFragment;
import products.dak.com.smartbank.fragments.Pembelian.TokenPlnFragment;
import products.dak.com.smartbank.fragments.Perbankan.TransferFragment;
import products.dak.com.smartbank.interfaces.AppBarControl;

public class SuperActivity extends AppCompatActivity implements AppBarControl {
    private static final String ARG_PARAM1 = "menu";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    private ImageView logoKanan,logoKiri,btnClose,btnBack;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_super);
            logoKanan = findViewById(R.id.logoAppBarRight);
            logoKiri = findViewById(R.id.logoAppBarLeft);
            btnBack = findViewById(R.id.btnBackAppBar);
            btnClose = findViewById(R.id.btnCloseAppBar);
            btnClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
            btnBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }
        System.out.println("asd asd resume");
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mParam1 = bundle.getString(ARG_PARAM1);
            mParam2 = bundle.getString(ARG_PARAM2);
        }
        if(mParam1.equalsIgnoreCase("mutasi")){
            System.out.println("asd masuk mutasi");
            MutasiFragment mutasiFragment = new MutasiFragment();
            addFragment(mutasiFragment);

        }else if(mParam1.equalsIgnoreCase("transfer")){
            System.out.println("asd masuk transfer");
            TransferFragment transferFragment = new TransferFragment();
            Bundle extras = new Bundle();
            extras.putString("step","1");
            transferFragment.setArguments(extras);
            addFragment(transferFragment);
        }else if(mParam1.equalsIgnoreCase("isi_pulsa")){
            System.out.println("asd masuk isi pulsa");
            PulsaFragment pulsaFragment = new PulsaFragment();
            Bundle extras = new Bundle();
            extras.putString("step","1");
            pulsaFragment.setArguments(extras);
            addFragment(pulsaFragment);
        }else if(mParam1.equalsIgnoreCase("post_paid")){
            System.out.println("asd masuk post paid");
            PostPaidFragment postPaidFragment = new PostPaidFragment();
            Bundle extras = new Bundle();
            extras.putString("step","1");
            postPaidFragment.setArguments(extras);
            addFragment(postPaidFragment);
        }else if(mParam1.equalsIgnoreCase("pln")){
            System.out.println("asd masuk PLN");
            TokenPlnFragment tokenPlnFragment = new TokenPlnFragment();
            Bundle extras = new Bundle();
            extras.putString("step","1");
            tokenPlnFragment.setArguments(extras);
            addFragment(tokenPlnFragment);
        }



    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void addFragment(Fragment fragment){
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.frame_layout_super,fragment);
        ft.commit();
    }


    @Override
    public void setBack() {
        logoKiri.setVisibility(View.INVISIBLE);
        btnBack.setVisibility(View.VISIBLE);
        btnClose.setVisibility(View.INVISIBLE);
        logoKanan.setVisibility(View.VISIBLE);
    }

    @Override
    public void setClose() {
        logoKiri.setVisibility(View.VISIBLE);
        btnBack.setVisibility(View.INVISIBLE);
        btnClose.setVisibility(View.VISIBLE);
        logoKanan.setVisibility(View.INVISIBLE);
    }

    @Override
    protected void onStart() {
        super.onStart();
        System.out.println("asd asd start");
    }

    @Override
    protected void onResume() {
        super.onResume();

    }


}
