package products.dak.com.smartbank.models.Transfer;

import java.io.Serializable;

public class Transfer implements Serializable {

    private String rc;
    private DataTransfer data;
    private String amount;
    private String destination;
    private String rm;
    private String msisdn;

    public Transfer(String rc, DataTransfer data, String amount, String destination, String rm, String msisdn) {
        this.rc = rc;
        this.data = data;
        this.amount = amount;
        this.destination = destination;
        this.rm = rm;
        this.msisdn = msisdn;
    }

    public String getRc() {
        return rc;
    }

    public DataTransfer getData() {
        return data;
    }

    public String getAmount() {
        return amount;
    }

    public String getDestination() {
        return destination;
    }

    public String getRm() {
        return rm;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public class DataTransfer{
        private String inq_id;
        private String random_digit_pin;
        private String date_time_trx;
        private String destination_name;

        public DataTransfer(String inq_id, String random_digit_pin, String date_time_trx, String destination_name) {
            this.inq_id = inq_id;
            this.random_digit_pin = random_digit_pin;
            this.date_time_trx = date_time_trx;
            this.destination_name = destination_name;
        }

        public String getInq_id() {
            return inq_id;
        }

        public String getRandom_digit_pin() {
            return random_digit_pin;
        }

        public String getDate_time_trx() {
            return date_time_trx;
        }

        public String getDestination_name() {
            return destination_name;
        }

        @Override
        public String toString() {
            return "DataTransfer{" +
                    "inq_id='" + inq_id + '\'' +
                    ", random_digit_pin='" + random_digit_pin + '\'' +
                    ", date_time_trx='" + date_time_trx + '\'' +
                    ", destination_name='" + destination_name + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "Transfer{" +
                "rc='" + rc + '\'' +
                ", data=" + data +
                ", amount='" + amount + '\'' +
                ", destination='" + destination + '\'' +
                ", rm='" + rm + '\'' +
                ", msisdn='" + msisdn + '\'' +
                '}';
    }
}
