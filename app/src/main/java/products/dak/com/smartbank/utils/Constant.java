package products.dak.com.smartbank.utils;

import android.graphics.Bitmap;

import org.json.JSONArray;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class Constant {
    public static String HTTP = "http://";

    //public static String IP = "172.168.100.58:8211";
    //public static String IP = "117.54.3.201:8010";
    //public static String IP = "103.103.192.134:80";
    //public static String IP = "proxy.dak.co.id";
    public static String IP = "172.16.100.13:8010";
    public static String SERVICE_URI = HTTP + IP + "/";
    public static String APPS = "crymob/ws_dev/";
    public static String APPS2 = "crymob/ws/";
    public static String URL_SERVICE = SERVICE_URI +APPS;
    public static String URL_SERVICE_SECURE = SERVICE_URI +APPS2;

    public static String KEY_DES_PIN = "MBDIGITALAMOREKRIYANESIA";
    public static String PARTNER_IDENTIFIER ="666";
    public static String PARTNER_AUTH ="modex";

    public static String URL_TESTING = "http://eagleschool.dx.am/ANDROID/";


    public static String TEXT_SHARE = "Sudah merasakan berbagai kemudahan dengan aplikasi LUPIRKA ?" +
            "Ajak orang-orang terdekat anda untuk ikut menikmati mudahnya mendapatkan";
    public static String TEXT_INTRO_CONTAC_US ="Jika anda memiliki kendala atau ada pertanyaan seputar event" +
            " silahkan hubungi kami";


    private static final String ALPHA_NUMERIC_STRING = "ABCDEF0123456789";
    public static String getKey(int count) {
        StringBuilder builder = new StringBuilder();
        while (count-- != 0) {
            int character = (int)(Math.random()*ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }
        return builder.toString();
    }

    public static JSONArray DATA_RESULT;

    public static String DATA_ALERT_RM,
            RESULT_SCAN_EKTP, EDITTEXT_NAME_NASABAH, EDITTEXT_NOREK_NASABAH,  EDITTEXT_NOHP_NASABAH;

    public static Bitmap RESULT_FOTO_DIRI;

}
