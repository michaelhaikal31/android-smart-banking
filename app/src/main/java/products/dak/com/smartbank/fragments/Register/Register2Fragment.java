package products.dak.com.smartbank.fragments.Register;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.Preference;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatDelegate;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import products.dak.com.smartbank.R;
import products.dak.com.smartbank.Sessions.UserSession;
import products.dak.com.smartbank.activities.main.FirstActivity;
import products.dak.com.smartbank.animations.MoveAnimation;
import products.dak.com.smartbank.utils.Constant;

public class Register2Fragment extends Fragment {
    private static final String ARG_PARAM1 = "data";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;

    private Button scanEKTP, fotoDiri;
    private EditText hasilScanEKTP, fullname, noRekening, noHandPhone;
    UserSession session = new UserSession();
    private ImageView imageView2;
    public Register2Fragment() {
        // Required empty public constructor
    }

    public static Register2Fragment newInstance(String param1, String param2) {
        Register2Fragment fragment = new Register2Fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_register2, container, false);
        ((FirstActivity) getActivity()).getSupportActionBar().show();
        ((FirstActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.colorDarkBlue), PorterDuff.Mode.SRC_ATOP);
        ((FirstActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(upArrow);

        setInitEx(view);
        return view;
    }

    public void setInitEx(View view) {

        scanEKTP = view.findViewById(R.id.scanEKTP);
        scanEKTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, 100);
                } else {
                        ScanKtpFragment scanKtpFragment = new ScanKtpFragment();
                        FragmentManager fm = getActivity().getSupportFragmentManager();
                        FragmentTransaction ft = fm.beginTransaction();
                        ft.replace(R.id.frame_layout1, scanKtpFragment);
                        ft.addToBackStack(null);
                        ft.commit();
                }
            }
        });

        fotoDiri = view.findViewById(R.id.fotoDiri);
        fotoDiri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    FotoDiriFragment fotoDiriFragment = new FotoDiriFragment();
                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    FragmentTransaction ft = fm.beginTransaction();
                    ft.replace(R.id.frame_layout1, fotoDiriFragment);
                    ft.addToBackStack(null);
                    ft.commit();
            }
        });
        fullname = view.findViewById(R.id.fullname);
        fullname.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_NEXT){
                    Constant.EDITTEXT_NAME_NASABAH = fullname.getText().toString();
                    System.out.println("asd 1 "+fullname.getText().toString());
                }
                return false;
            }
        });
        fullname.setText(Constant.EDITTEXT_NAME_NASABAH);

        noRekening = view.findViewById(R.id.noRekening);
        noRekening.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_NEXT){
                    Constant.EDITTEXT_NOREK_NASABAH = noRekening.getText().toString();
                    System.out.println("asd 2 "+noRekening.getText().toString());
                }
                return false;
            }
        });
        noRekening.setText(Constant.EDITTEXT_NOREK_NASABAH);

        noHandPhone = view.findViewById(R.id.noHandPhone);
        noHandPhone.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_NEXT){
                    Constant.EDITTEXT_NOHP_NASABAH = noHandPhone.getText().toString();
                    System.out.println("asd 3 "+noHandPhone.getText().toString());
                }
                return false;
            }
        });
        noHandPhone.setText(Constant.EDITTEXT_NOHP_NASABAH);

        hasilScanEKTP = view.findViewById(R.id.hasilScanEKTP);
        hasilScanEKTP.setText(Constant.RESULT_SCAN_EKTP);
        imageView2 = view.findViewById(R.id.imageView2);
        imageView2.setImageBitmap(Constant.RESULT_FOTO_DIRI);

    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();

            hasilScanEKTP.setText(Constant.RESULT_SCAN_EKTP);

        System.out.println(Constant.RESULT_SCAN_EKTP);
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {

            return MoveAnimation.create(MoveAnimation.RIGHT, enter, 300);

    }

}
