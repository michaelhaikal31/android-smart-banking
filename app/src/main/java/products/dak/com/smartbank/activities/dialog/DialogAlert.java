package products.dak.com.smartbank.activities.dialog;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import products.dak.com.smartbank.R;
import products.dak.com.smartbank.utils.Constant;

public class DialogAlert extends DialogFragment implements View.OnClickListener {
    private TextView tvValueDialog;
    public Context activity;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.activity = context;
    }

    @Override
    public Dialog onCreateDialog( Bundle savedInstanceState) {
        final Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        return  dialog;
    }


    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.dialog_alert, container);
        view.findViewById(R.id.btnDialog).setOnClickListener(this);
        tvValueDialog = view.findViewById(R.id.tvValueDialog);
        tvValueDialog.setText(Constant.DATA_ALERT_RM);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnDialog :
                dismissAllowingStateLoss();
                break;
            default:
                throw new IllegalArgumentException("Unrecognized id");
        }
    }

    public void show(FragmentManager fragmentManager, String alert) {
        super.show(fragmentManager, alert);
    }

}
