package products.dak.com.smartbank.fragments.Perbankan;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import products.dak.com.smartbank.R;
import products.dak.com.smartbank.Sessions.UserSession;
import products.dak.com.smartbank.animations.MoveAnimation;
import products.dak.com.smartbank.fragments.Otp.OtpVerificationFragment;
import products.dak.com.smartbank.interfaces.ApiService;
import products.dak.com.smartbank.interfaces.AppBarControl;
import products.dak.com.smartbank.models.Mutasi.Mutasi;
import products.dak.com.smartbank.models.Transfer.Transfer;
import products.dak.com.smartbank.models.Transfer.TransferCheck;
import products.dak.com.smartbank.services.RetrofitService;
import products.dak.com.smartbank.utils.Constant;
import products.dak.com.smartbank.utils.Function;
import products.dak.com.smartbank.utils.TripleDes;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class TransferFragment extends Fragment {
    private static final String ARG_PARAM1 = "step";
    private static final String ARG_PARAM2 = "no_rek";
    private static final String ARG_PARAM3 = "nama";
    private static final String ARG_PARAM4 = "nominal";
    private static final String ARG_PARAM5 = "pesan";
    private static final String ARG_PARAM6 = "inq_id";
    private String mParam1;
    private String mParam2;
    private String mParam3;
    private String mParam4;
    private String mParam5;
    private String mParam6;
    private Transfer transfer;
    private Button btnTransfer;
    private EditText txtNominal,txtPesan,txtNoRek,txtPinTransfer;
    private TextView txtKeterangan,txtNoTujuan,txtNamaPenerima,txtNominal2,txtPesan2,txtPerintahPin,txtTitle;
    private TextInputLayout textInputLayout1,textInputLayout2;
    private boolean verified=false;
    private JSONObject paramObject;
    private TripleDes tripleDesJSon;
    private String encryptedBody;
    private List<Mutasi> listMutasi;
    private ProgressDialog progressDialog;
    AppBarControl appBarControl;

    public TransferFragment() {

    }


    public static TransferFragment newInstance(String param1, String param2,String param3,String param4,String param5,String param6) {
        TransferFragment fragment = new TransferFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        args.putString(ARG_PARAM3, param3);
        args.putString(ARG_PARAM4, param4);
        args.putString(ARG_PARAM5, param5);
        args.putString(ARG_PARAM6, param6);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            mParam3 = getArguments().getString(ARG_PARAM3);
            mParam4 = getArguments().getString(ARG_PARAM4);
            mParam5 = getArguments().getString(ARG_PARAM5);
            transfer = (Transfer)getArguments().getSerializable("transferData");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = null;
        if(mParam1.equalsIgnoreCase("2")) {
            v = inflater.inflate(R.layout.fragment_transfer_step_two,container,false);
            btnTransfer=v.findViewById(R.id.btnTransfer2);
            txtNoTujuan = v.findViewById(R.id.txtNoRekTujuanTransfer);
            txtNamaPenerima = v.findViewById(R.id.txtNamaRekTujuanTransfer);
            txtNominal2 = v.findViewById(R.id.txtNominalTransfer);
            txtPesan2 = v.findViewById(R.id.txtPesanTransfer);
            txtPerintahPin = v.findViewById(R.id.txtPerintahInputPinTransfer);
            txtPinTransfer = v.findViewById(R.id.txtPinTransfer);

            txtPerintahPin.setText("Masukan Digit ke-["+transfer.getData().getRandom_digit_pin().charAt(0)+"] dan Digit ke-["+transfer.getData().getRandom_digit_pin().charAt(1)+"] dari PIN anda !");
            txtNoTujuan.setText(transfer.getDestination());
            txtNamaPenerima.setText(transfer.getData().getDestination_name());
            txtNominal2.setText(transfer.getAmount());
            txtPesan2.setText(mParam5);
            btnTransfer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    progressDialog = new ProgressDialog(getActivity());
                    progressDialog.setMessage("Please wait");
                    progressDialog.show();
                    conf_transfer();
                }
            });

        }else{
            v = inflater.inflate(R.layout.fragment_transfer_step_one, container, false);
            txtTitle = v.findViewById(R.id.textView23);
            btnTransfer = v.findViewById(R.id.btnTransfer1);
            txtNoRek = v.findViewById(R.id.txtNoRekTransfer);
            txtNominal = v.findViewById(R.id.txtNominalTransfer);
            txtPesan = v.findViewById(R.id.txtPesanTransfer);
            txtKeterangan = v.findViewById(R.id.txtKeteranganTransfer);
            textInputLayout1 = v.findViewById(R.id.textInputLayout2);
            textInputLayout2 = v.findViewById(R.id.textInputLayout3);

            txtTitle.setVisibility(View.INVISIBLE);
            txtNominal.setVisibility(View.INVISIBLE);
            txtPesan.setVisibility(View.INVISIBLE);
            txtKeterangan.setVisibility(View.INVISIBLE);
            textInputLayout1.setVisibility(View.INVISIBLE);
            textInputLayout2.setVisibility(View.INVISIBLE);

            btnTransfer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                if(verified){
                    if(txtNominal.getText().length()==0){
                        txtNominal.setError("Mohon isi nominal transfer");
                    }else {
                        progressDialog = new ProgressDialog(getActivity());
                        progressDialog.setMessage("Please wait");
                        progressDialog.show();
                        transfer();
                    }
                }else{
                    if(txtNoRek.getText().length()==0){
                        txtNoRek.setError("Mohon isi no rekening tujuan !");
                    }else {
                        progressDialog = new ProgressDialog(getActivity());
                        progressDialog.setMessage("Please wait");
                        progressDialog.show();
                        check_no_rek();
                    }
                }
                }
            });


        }
        return v;
    }

    private void check_no_rek(){
        final String key = Constant.getKey(24);
        ApiService apiInterface = RetrofitService.getRetrofit(key).create(ApiService.class);
        paramObject = new JSONObject();
        final UserSession userSession = new UserSession();
        try{
            tripleDesJSon = new TripleDes(key);
            paramObject.put("msisdn",userSession.getMsiSdn(getActivity()));
            paramObject.put("destination",txtNoRek.getText());
            paramObject.put("jenis_tabungan",userSession.getJenisTabungan(getActivity()));
            encryptedBody = tripleDesJSon.encrypt(paramObject.toString());
        }catch (JSONException e){
        }catch (Exception e){

        }
        System.out.println("asd [TranferFragment] paramObject check no rek"+paramObject.toString());
        Call<String> call = apiInterface.cek_noRekening(encryptedBody);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try{
                    String resultDecrypt = tripleDesJSon.decrypt(response.body());
                    System.out.println("asd [TranferFragment] response"+resultDecrypt);
                    TransferCheck transferCheck = new Gson().fromJson(resultDecrypt,TransferCheck.class);
                    if(transferCheck.getRm().equalsIgnoreCase("Success")){
                        txtKeterangan.setText(transferCheck.getData().getName());
                        txtTitle.setVisibility(View.VISIBLE);
                        txtNominal.setVisibility(View.VISIBLE);
                        txtPesan.setVisibility(View.VISIBLE);
                        txtKeterangan.setVisibility(View.VISIBLE);
                        textInputLayout1.setVisibility(View.VISIBLE);
                        textInputLayout2.setVisibility(View.VISIBLE);
                        verified=true;
                    }else{
                        Function.showAlert(getActivity(),transferCheck.getRm());
//                        Snackbar.make(getView(), transferCheck.getRm(), Snackbar.LENGTH_SHORT).show();
                    }

                }catch(Exception e){
                    System.out.println("asd catch "+e.toString());
                    Snackbar.make(getView(),"Cannot processing server's response !",Snackbar.LENGTH_SHORT).show();
                }
                progressDialog.dismiss();
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Snackbar.make(getView(),"Oops, Something wrong please try again later",Snackbar.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    private void transfer(){
        final String key = Constant.getKey(24);
        ApiService apiInterface = RetrofitService.getRetrofit(key).create(ApiService.class);
        paramObject = new JSONObject();
        final UserSession userSession = new UserSession();
        try{
            tripleDesJSon = new TripleDes(key);
            paramObject.put("msisdn",userSession.getMsiSdn(getActivity()));
            paramObject.put("destination",txtNoRek.getText());
            paramObject.put("jenis_tabungan",userSession.getJenisTabungan(getActivity()));
            paramObject.put("amount",txtNominal.getText());
            encryptedBody = tripleDesJSon.encrypt(paramObject.toString());
        }catch (JSONException e){
        }catch (Exception e){

        }
        System.out.println("asd [TranferFragment] [paramObject Tranfer] "+paramObject);
        Call<String> call = apiInterface.transfer(encryptedBody);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try{
                    //Toast.makeText(getActivity(),response.body().toString(),Toast.LENGTH_SHORT).show();;
                    Transfer transfer = new Gson().fromJson(tripleDesJSon.decrypt(response.body()),Transfer.class);
                    if(transfer.getRm().equalsIgnoreCase("Success")){
                        TransferFragment transferFragment = new TransferFragment();
                        Bundle extras  = new Bundle();
                        extras.putString("step","2");
                        extras.putString("pesan",txtPesan.getText().toString());
                        extras.putSerializable("transferData",transfer);
                        transferFragment.setArguments(extras);
                        FragmentManager fm = getActivity().getSupportFragmentManager();
                        FragmentTransaction ft = fm.beginTransaction();
                        ft.replace(R.id.frame_layout_super,transferFragment);
                        ft.addToBackStack("TransferStepOne");
                        ft.commit();
                    }else{
                        Snackbar.make(getView(),transfer.getRm(),Snackbar.LENGTH_SHORT).show();
                    }

                }catch(Exception e){
                    System.out.println("asd catch "+e.toString());
                    Snackbar.make(getView(),"Cannot processing server's response !",Snackbar.LENGTH_SHORT).show();
                }
                progressDialog.dismiss();
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Snackbar.make(getView(),"Oops, Something wrong please try again later",Snackbar.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });


    }

    private void conf_transfer(){
        final String key = Constant.getKey(24);
        ApiService apiInterface = RetrofitService.getRetrofit(key).create(ApiService.class);
        paramObject = new JSONObject();
        final UserSession userSession = new UserSession();
        try{
            tripleDesJSon = new TripleDes(key);
            paramObject.put("msisdn",userSession.getMsiSdn(getActivity()));
            paramObject.put("destination",txtNoTujuan.getText());
            paramObject.put("amount",txtNominal2.getText());
            paramObject.put("random_digit_pin",transfer.getData().getRandom_digit_pin());
            paramObject.put("pin",txtPinTransfer.getText());
            paramObject.put("inq_id",transfer.getData().getInq_id());
            encryptedBody = tripleDesJSon.encrypt(paramObject.toString());
        }catch (JSONException e){
            System.out.println("asd catch "+e.toString());
        }catch (Exception e){
            System.out.println("asd exception "+e.toString());

        }
        System.out.println("asd [TranferFragment] [ConfTranfer] "+paramObject);
        userSession.setTripleDesKey(getActivity(),key);
        userSession.setEncryptedBody(getActivity(),encryptedBody);
        Call<String> call = apiInterface.conf_transfer(encryptedBody);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try{
                    System.out.println("asd transfer respon  "+tripleDesJSon.decrypt(response.body()));
                    System.out.println("asd transfer message "+tripleDesJSon.decrypt(response.message() ));
                    //Toast.makeText(getActivity(),response.body().toString(),Toast.LENGTH_SHORT).show();;
                    Transfer transfer = new Gson().fromJson(tripleDesJSon.decrypt(response.body()),Transfer.class);
                    if(transfer.getRm().equalsIgnoreCase("Success")){
                        userSession.setEncryptedBody(getActivity(),encryptedBody);
                        userSession.setTripleDesKey(getActivity(),key);
                        OtpVerificationFragment otpVerificationFragment = new OtpVerificationFragment();
                        Bundle extras  = new Bundle();
                        extras.putInt("code",2);
                        otpVerificationFragment.setArguments(extras);
                        FragmentManager fm = getActivity().getSupportFragmentManager();
                        FragmentTransaction ft = fm.beginTransaction();
                        ft.replace(R.id.frame_layout_super,otpVerificationFragment);
                        ft.commit();
                    }else{
                        Snackbar.make(getView(),transfer.getRm(),Snackbar.LENGTH_SHORT).show();
                    }

                }catch(Exception e){
                    System.out.println("asd catch "+e.toString());
                    Snackbar.make(getView(),"Cannot processing server's response !",Snackbar.LENGTH_SHORT).show();
                }
                progressDialog.dismiss();
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                System.out.println("asd throw "+t.toString());
                Snackbar.make(getView(),"Oops, Something wrong please try again later",Snackbar.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });



    }

    /*

     TransferFragment transferFragment = new TransferFragment();
                    Bundle extras  = new Bundle();
                    extras.putString("step","2");
                    extras.putString("no_rek",txtNoRek.getText().toString());
                    transferFragment.setArguments(extras);
                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    FragmentTransaction ft = fm.beginTransaction();
                    ft.replace(R.id.frame_layout_super,transferFragment);
                    ft.commit();

     */


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            appBarControl = (AppBarControl) context;
            appBarControl.setBack();
        } catch (ClassCastException castException) {
            /** The activity does not implement the listener. */
        }

    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if(mParam1.equalsIgnoreCase("1")){
            return MoveAnimation.create(MoveAnimation.RIGHT, enter, 300);
        }else{
            return MoveAnimation.create(MoveAnimation.LEFT, enter, 300);
        }
    }
}
