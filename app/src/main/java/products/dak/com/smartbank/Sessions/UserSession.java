package products.dak.com.smartbank.Sessions;

import android.content.Context;
import android.content.SharedPreferences;

import products.dak.com.smartbank.utils.Constant;

public class UserSession {
    public void setName(Context context, String value) {
        SharedPreferences.Editor editor = context.getSharedPreferences("userSession", Context.MODE_PRIVATE).edit();
        editor.putString("name", value);
        editor.commit();

    }

    public  String getName(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("userSession",	Context.MODE_PRIVATE);
        String nma = prefs.getString("name", "");
        return nma;
    }

    public void setMsiSdn(Context context, String value) {
        SharedPreferences.Editor editor = context.getSharedPreferences("userSession", Context.MODE_PRIVATE).edit();
        editor.putString("msisdn", value);
        editor.commit();

    }

    public  String getMsiSdn(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("userSession",	Context.MODE_PRIVATE);
        String nma = prefs.getString("msisdn", "");
        return nma;
    }

    public void clearSession(Context context){
        SharedPreferences.Editor editor = context.getSharedPreferences("userSession",Context.MODE_PRIVATE).edit().clear();
        editor.commit();
    }

    public String getEncryptedBody(Context context){
        SharedPreferences prefs = context.getSharedPreferences("userSession",	Context.MODE_PRIVATE);
        String data = prefs.getString("stringRequest","");
        return data;
    }

    public void setEncryptedBody(Context context,String value){
        SharedPreferences.Editor editor = context.getSharedPreferences("userSession",Context.MODE_PRIVATE).edit();
        editor.putString("stringRequest", value);
        editor.commit();
    }

    public void setTripleDesKey(Context context,String value){
        SharedPreferences.Editor editor = context.getSharedPreferences("userSession",Context.MODE_PRIVATE).edit();
        editor.putString("key", value);
        editor.commit();
    }

    public String getTripleDesKey(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("userSession", Context.MODE_PRIVATE);
        String data = prefs.getString("key", "");
        return data;
    }

    public void setJenisTabungan(Context context,String jenis){
        SharedPreferences.Editor editor = context.getSharedPreferences("userSession",Context.MODE_PRIVATE).edit();
        editor.putString("tabungan", jenis);
        editor.commit();
    }

    public String getJenisTabungan(Context context){
        SharedPreferences prefs = context.getSharedPreferences("userSession", Context.MODE_PRIVATE);
        String data = prefs.getString("tabungan", "");
        return data;
    }


}
