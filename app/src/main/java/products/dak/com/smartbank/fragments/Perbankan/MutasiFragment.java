package products.dak.com.smartbank.fragments.Perbankan;


import android.app.ProgressDialog;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.gson.Gson;
import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.IAdapter;
import com.mikepenz.fastadapter.adapters.FastItemAdapter;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import products.dak.com.smartbank.R;
import products.dak.com.smartbank.Sessions.UserSession;
import products.dak.com.smartbank.interfaces.ApiService;
import products.dak.com.smartbank.models.Mutasi.Mutasi;
import products.dak.com.smartbank.models.Mutasi.ValueMutasi;
import products.dak.com.smartbank.services.RetrofitService;
import products.dak.com.smartbank.utils.Constant;
import products.dak.com.smartbank.utils.TripleDes;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MutasiFragment extends Fragment implements DatePickerDialog.OnDateSetListener {


    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    private RecyclerView recyclerView;
    private FastItemAdapter<Mutasi> fastAdapter;
    private JSONObject paramObject;
    private TripleDes tripleDesJSon;
    private String encryptedBody;
    private List<Mutasi> listMutasi;
    private ProgressDialog progressDialog;
    private Button btnFrom,btnTo;
    private int type;
    private String firstDate="",secondDate="";

    public MutasiFragment() {

    }

    public static MutasiFragment newInstance(String param1, String param2) {
        MutasiFragment fragment = new MutasiFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_mutasi, container, false);
        recyclerView = v.findViewById(R.id.recycleMutasi);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        btnFrom = v.findViewById(R.id.btnFrom);
        btnTo = v.findViewById(R.id.btnTo);
        btnFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type=1;
                show_dialog();
            }
        });
        btnTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type=2;
                show_dialog();
            }
        });


// Fetch your data here
//        progressDialog = new ProgressDialog(getActivity());
//        progressDialog.setMessage("Please wait");
//        progressDialog.show();
//        get_mutasi();
        //loadMutasi();



        //

        /*
        recyclerView = v.findViewById(R.id.recycleMutasi);
        listMutasi = new ArrayList<>();
        get_mutasi();
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false));
        fastItemAdapter = new FastItemAdapter<>();
        recyclerView.setAdapter(fastItemAdapter);

       progressDialog = new ProgressDialog(getActivity());
       progressDialog.setMessage("Please wait");
       progressDialog.show();
       */

        /*
        fastItemAdapter.withOnClickListener(new FastAdapter.OnClickListener<Menu>() {
            @Override
            public boolean onClick(View v, IAdapter<Menu> adapter, Menu item, int position) {
                DetailFragment detailFragment = new DetailFragment();
                Bundle extras  = new Bundle();
                extras.putString("type","1");
                extras.putString("param2",listMutasi.get(position).getTrx_reff());
                detailFragment.setArguments(extras);
                FragmentManager fm = getActivity().getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.frame_layout_super,detailFragment);
                ft.commit();

                return false;
            }
        });
           */

        return v;

    }

    private DatePickerDialog create_dialog(){
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait");
        progressDialog.show();
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                MutasiFragment.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        progressDialog.dismiss();
        return dpd;
    }

    private void show_dialog(){
       create_dialog();
        create_dialog().show(getActivity().getFragmentManager(), "Datepickerdialog");
    }


    private void loadMutasi(){
        get_mutasi();
        System.out.println("asd asd "+listMutasi.toString());
    }

    private void get_mutasi(){
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait");
        progressDialog.show();
        final String key = Constant.getKey(24);
        ApiService apiInterface = RetrofitService.getRetrofit(key).create(ApiService.class);
        paramObject = new JSONObject();
        final UserSession userSession = new UserSession();
        try{
            tripleDesJSon = new TripleDes(key);
            paramObject.put("msisdn",userSession.getMsiSdn(getActivity()));
            paramObject.put("jenis_tabungan",userSession.getJenisTabungan(getActivity()));
            paramObject.put("start_date",firstDate);
            paramObject.put("end_date",secondDate);
            encryptedBody = tripleDesJSon.encrypt(paramObject.toString());
        }catch (JSONException e){
        }catch (Exception e){

        }
        System.out.println("asd [MutasiFragment] [paramObject getMutasi] "+paramObject);
        Call<String> call = apiInterface.cek_mutasi(encryptedBody);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try{
                    String resultDecrypt = tripleDesJSon.decrypt(response.body());
                    System.out.println("asd [MutasiFragment] [result Decrypt] "+resultDecrypt);
                    listMutasi = new ArrayList<>();
                    ValueMutasi valueMutasi = new Gson().fromJson(resultDecrypt,ValueMutasi.class);
                    if(valueMutasi.rc.equalsIgnoreCase("00")){
                       for(int i=0;i<valueMutasi.getData().size();i++){
                           listMutasi.add(new Mutasi(valueMutasi.getData().get(i).getAmount(),
                                   valueMutasi.getData().get(i).getDate_time_trx(),
                                   valueMutasi.getData().get(i).getTrx_type(),
                                   valueMutasi.getData().get(i).getTrx(),
                                   valueMutasi.getData().get(i).getTrx_reff()
                                   ));
                       }
                        System.out.println("asd nih "+listMutasi.toString());
                    }else{
                        Snackbar.make(getView(),valueMutasi.rm,Snackbar.LENGTH_SHORT).show();
                    }

                }catch (Exception e){
                    System.out.println("asd catch "+e.toString());
                    Snackbar.make(getView(),"Cannot processing server's response !",Snackbar.LENGTH_SHORT).show();
                }

                fastAdapter = new FastItemAdapter<>();
                recyclerView.setAdapter(fastAdapter);
                fastAdapter.withSelectable(true);
                fastAdapter.withOnClickListener(new FastAdapter.OnClickListener<Mutasi>() {
                    @Override
                    public boolean onClick(View v, IAdapter<Mutasi> adapter, Mutasi item, int position) {
                        // Handle click here
                        DetailFragment detailFragment = new DetailFragment();
                        Bundle extras  = new Bundle();
                        extras.putInt("type",1);
                        extras.putString("mParam2",listMutasi.get(position).getTrx_reff());
                        detailFragment.setArguments(extras);
                        FragmentManager fm = getActivity().getSupportFragmentManager();
                        FragmentTransaction ft = fm.beginTransaction();
                        ft.replace(R.id.frame_layout_super,detailFragment);
                        ft.addToBackStack(null);
                        ft.commit();
                        return true;
                    }
                });
                fastAdapter.add(listMutasi);
                progressDialog.dismiss();
                progressDialog.dismiss();
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Snackbar.make(getView(),"Oops, Something wrong please try again later",Snackbar.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }


    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date,tgl="",bln="";
        monthOfYear++;

        tgl = String.format("%02d", dayOfMonth);
        bln = String.format("%02d", monthOfYear);

        if(type==1) {
            btnFrom.setText(tgl+"/"+bln+"/"+year);
            firstDate=tgl+bln+year;
        }else{
            btnTo.setText(tgl+"/"+bln+"/"+year);
            secondDate=tgl+bln+year;
        }
        if(!firstDate.equalsIgnoreCase("") && !secondDate.equalsIgnoreCase("")){
            System.out.println("asd [MutasiFragment] [getFirsDate] "+firstDate);
            get_mutasi();
        }

    }
}
