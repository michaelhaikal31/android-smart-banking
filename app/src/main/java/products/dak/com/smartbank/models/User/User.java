package products.dak.com.smartbank.models.User;

public class User {
public String name;
public String status_user;
public String pin;


    public User() {
    }

    public User(String name, String status_user) {
        this.name = name;
        this.status_user = status_user;
    }

    public String getName() {
        return name;
    }

    public String getStatus_user() {
        return status_user;
    }

    public String getPin() {
        return pin;
    }
}
