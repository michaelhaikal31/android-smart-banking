package products.dak.com.smartbank.models.User;

import products.dak.com.smartbank.models.BaseResponse;

public class ValueUser extends BaseResponse {

    private String pin;
    private User data;
    private String flag_reset_password;

    public String getPin() {
        return pin;
    }
    public User getData() {
        return data;
    }
    public String getFlag_reset_password() {
        return flag_reset_password;
    }

}
