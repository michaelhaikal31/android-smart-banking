package products.dak.com.smartbank.models.Transfer;

public class TransferCheck {

    private String rc;
    private DataTransferCheck data;
    private String destination;
    private String rm;
    private String msisdn;

    public TransferCheck(String rc, DataTransferCheck data, String destination, String rm, String msisdn) {
        this.rc = rc;
        this.data = data;
        this.destination = destination;
        this.rm = rm;
        this.msisdn = msisdn;
    }

    public String getRc() {
        return rc;
    }

    public DataTransferCheck getData() {
        return data;
    }

    public String getDestination() {
        return destination;
    }

    public String getRm() {
        return rm;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public class DataTransferCheck{
        private String rekening_no;
        private String name;

        public DataTransferCheck(String rekening_no, String name) {
            this.rekening_no = rekening_no;
            this.name = name;
        }

        public String getRekening_no() {
            return rekening_no;
        }

        public String getName() {
            return name;
        }
    }
}
