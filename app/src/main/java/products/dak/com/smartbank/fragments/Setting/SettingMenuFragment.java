package products.dak.com.smartbank.fragments.Setting;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import products.dak.com.smartbank.R;
import products.dak.com.smartbank.Sessions.UserSession;
import products.dak.com.smartbank.activities.main.FirstActivity;
import products.dak.com.smartbank.fragments.Otp.OtpVerificationFragment;
import products.dak.com.smartbank.interfaces.ApiService;
import products.dak.com.smartbank.interfaces.AppBarControl;
import products.dak.com.smartbank.models.Setting.Setting;
import products.dak.com.smartbank.models.User.ValueUser;
import products.dak.com.smartbank.services.RetrofitService;
import products.dak.com.smartbank.utils.Constant;
import products.dak.com.smartbank.utils.TripleDes;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SettingMenuFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private Button btnOne, btnTwo, btnThree, btnFour;
    private EditText txtOldNumber, txtNewNumber, txtConfirmation;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String mParam3;
    private ProgressDialog progressDialog;
    private JSONObject paramObject;
    private String encryptedBody;
    private TripleDes tripleDesPin, tripleDesJson;
    private AppBarControl appBarControl;

    public SettingMenuFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create icon_change_pin new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SettingMenuFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SettingMenuFragment newInstance(String param1, String param2, String param3) {
        SettingMenuFragment fragment = new SettingMenuFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        args.putString(ARG_PARAM3, param3);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            mParam3 = getArguments().getString(ARG_PARAM3);
        } else {
            mParam1 = "asd";
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = null;
        if (mParam1.equalsIgnoreCase("formChangePhone")) {
            v = inflater.inflate(R.layout.fragment_change_phone_number, container, false);
            appBarControl = (AppBarControl) getActivity();
            appBarControl.setBack();
            txtNewNumber = v.findViewById(R.id.txtNewNumber);
            btnOne = v.findViewById(R.id.btnGanti);
            btnOne.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    progressDialog = new ProgressDialog(getActivity());
                    progressDialog.setMessage("Please wait");
                    progressDialog.show();
                    change_phone();
                }
            });

        } else if (mParam1.equalsIgnoreCase("formChangePassword")) {
            v = inflater.inflate(R.layout.fragment_change_password, container, false);
            appBarControl = (AppBarControl) getActivity();
            appBarControl.setBack();
            //if Change Pin = 1
            if (mParam3 == "show") {
                DialogShow();
            }

            txtOldNumber = v.findViewById(R.id.txtOldNumber);
            txtNewNumber = v.findViewById(R.id.txtNewNumber);
            txtConfirmation = v.findViewById(R.id.txtConfirmationNumber);
            btnOne = v.findViewById(R.id.btnGanti);
            btnOne.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    progressDialog = new ProgressDialog(getActivity());
                    progressDialog.setMessage("Please wait");
                    progressDialog.show();
                    change_pin();
                    /*
                    Login login = new Login();
                    Bundle extras = new Bundle();
                    extras.putString("jenis", "changePassword");
                    login.setArguments(extras);
                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    FragmentTransaction ft = fm.beginTransaction();
                    ft.replace(R.id.frame_layout_setting, login,"menuChangePhone");
                    ft.commit();
                    */
                }
            });

        } else {
            v = inflater.inflate(R.layout.fragment_setting_menu, container, false);
            btnOne = v.findViewById(R.id.btnChangePhoneNumber);
            btnOne.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SettingMenuFragment settingMenuFragment = new SettingMenuFragment();
                    Bundle extras = new Bundle();
                    extras.putString("param1", "formChangePhone");
                    settingMenuFragment.setArguments(extras);
                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    FragmentTransaction ft = fm.beginTransaction();
                    ft.replace(R.id.frame_layout_setting, settingMenuFragment, "menuChangePassword");
                    ft.addToBackStack(null);
                    ft.commit();
                }
            });
            btnTwo = v.findViewById(R.id.btnChangePassword);
            btnTwo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SettingMenuFragment settingMenuFragment = new SettingMenuFragment();
                    Bundle extras = new Bundle();
                    extras.putString("param1", "formChangePassword");
                    settingMenuFragment.setArguments(extras);
                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    FragmentTransaction ft = fm.beginTransaction();
                    ft.replace(R.id.frame_layout_setting, settingMenuFragment);
                    ft.addToBackStack(null);
                    ft.commit();
                }
            });
            btnThree = v.findViewById(R.id.btnHubungiKami);
            btnThree.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    LayoutInflater inflater = getLayoutInflater();
                    View view = inflater.inflate(R.layout.dialog_hubungi_kami, null);
                    Button btnHubungi = view.findViewById(R.id.btnDialogSetting);
                    btnHubungi.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(Intent.ACTION_CALL);
                            intent.setData(Uri.parse("tel:+628112206411"));
                            startActivity(intent);
                        }
                    });
                    alert.setView(view);
                    alert.create();
                    alert.show();
                }
            });
            btnFour = v.findViewById(R.id.btnHapusAkun);
            btnFour.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    LayoutInflater inflater = getLayoutInflater();
                    View view = inflater.inflate(R.layout.dialog_hapus_akun, null);
                    Button btnTidak = view.findViewById(R.id.btnTidak);
                    Button btnYa = view.findViewById(R.id.btnYa);
                    alert.setView(view);
                    final AlertDialog dialog = alert.create();

                    btnTidak.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    btnYa.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            progressDialog = new ProgressDialog(getActivity());
                            progressDialog.setMessage("Please wait");
                            progressDialog.show();
                            hapus_akun();
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                }
            });

        }
        return v;
    }

    private void DialogShow() {
        //appear dialog "inputkan pin baru"
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        alertDialogBuilder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialogBuilder.setTitle(getString(R.string.dialog_title_changepin));
        alertDialogBuilder.setMessage(getString(R.string.dialog_input_pin_baru));
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    private void hapus_akun() {
        final String key = Constant.getKey(24);
        ApiService apiInterface = RetrofitService.getRetrofit(key).create(ApiService.class);
        paramObject = new JSONObject();
        final UserSession userSession = new UserSession();
        try {
            tripleDesJson = new TripleDes(key);
            paramObject.put("msisdn", userSession.getMsiSdn(getActivity()));
            encryptedBody = tripleDesJson.encrypt(paramObject.toString());
        } catch (JSONException e) {

        } catch (Exception e) {

        }
        System.out.println("asd " + paramObject.toString());
        Call<String> call = apiInterface.delete_account(encryptedBody);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try {
                    //Toast.makeText(getActivity(),response.body().toString(),Toast.LENGTH_SHORT).show();
                    ValueUser valueUser = new Gson().fromJson(tripleDesJson.decrypt(response.body()), ValueUser.class);
                    if (valueUser.getRc().equalsIgnoreCase("00")) {
                        Snackbar.make(getView(), "Yout account was successfully deleted !", Snackbar.LENGTH_SHORT).show();
                        Intent i = new Intent(getActivity(), FirstActivity.class);
                        userSession.clearSession(getActivity());
                        startActivity(i);
                        getActivity().finish();
                    } else {
                        Snackbar.make(getView(), valueUser.getRm(), Snackbar.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    System.out.println("asd catch " + e.toString());
                    Snackbar.make(getView(), "Cannot processing server's response !", Snackbar.LENGTH_SHORT).show();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Snackbar.make(getView(), "Oops, Something wrong please try again later", Snackbar.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    private void change_pin() {
        final String key = Constant.getKey(24);
        ApiService apiInterface = RetrofitService.getRetrofit(key).create(ApiService.class);
        paramObject = new JSONObject();
        final UserSession userSession = new UserSession();
        try {
            tripleDesPin = new TripleDes(Constant.KEY_DES_PIN);
            tripleDesJson = new TripleDes(key);
            if (mParam2 == null) {
                paramObject.put("msisdn", userSession.getMsiSdn(getActivity()));

            } else {
                paramObject.put("msisdn", mParam2);

            }
            paramObject.put("old_pin", tripleDesPin.encrypt(txtOldNumber.getText().toString()));
            paramObject.put("new_pin", tripleDesPin.encrypt(txtNewNumber.getText().toString()));
            paramObject.put("conf_new_pin", tripleDesPin.encrypt(txtConfirmation.getText().toString()));
            encryptedBody = tripleDesJson.encrypt(paramObject.toString());
        } catch (JSONException e) {

        } catch (Exception e) {

        }
        System.out.println("asd " + paramObject.toString());
        userSession.setEncryptedBody(getActivity(), encryptedBody);
        userSession.setTripleDesKey(getActivity(), key);
        Call<String> call = apiInterface.change_pin(encryptedBody);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try {
                    //Toast.makeText(getActivity(),response.body().toString(),Toast.LENGTH_SHORT).show();
                    Setting.changePin changePin = new Gson().fromJson(tripleDesJson.decrypt(response.body()), Setting.changePin.class);
                    if (changePin.rm.equalsIgnoreCase("Success")) {
                        OtpVerificationFragment otpVerificationFragment = new OtpVerificationFragment();
                        Bundle extras = new Bundle();
                        extras.putInt("code", 5);
                        extras.putString("msisdn", mParam2);
                        otpVerificationFragment.setArguments(extras);
                        FragmentManager fm = getActivity().getSupportFragmentManager();
                        FragmentTransaction ft = fm.beginTransaction();
                        ft.replace(R.id.frame_layout_setting, otpVerificationFragment);
                        ft.commit();
                    } else {
                        Snackbar.make(getView(), changePin.rm, Snackbar.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    System.out.println("asd catch " + e.toString());
                    Snackbar.make(getView(), "Cannot processing server's response !", Snackbar.LENGTH_SHORT).show();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                System.out.println("asd" + call.toString() + "|" + t.toString());
                Snackbar.make(getView(), "Oops, Something wrong please try again later", Snackbar.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    private void change_phone() {
        final String key = Constant.getKey(24);
        ApiService apiInterface = RetrofitService.getRetrofit(key).create(ApiService.class);
        paramObject = new JSONObject();
        final UserSession userSession = new UserSession();
        try {
            tripleDesJson = new TripleDes(key);
            String fixNumber = String.valueOf(txtNewNumber.getText().toString().charAt(0));
            if (fixNumber.equalsIgnoreCase("0")) {
                StringBuilder newPhone = new StringBuilder(txtNewNumber.getText());
                newPhone.deleteCharAt(0);
                fixNumber = "62" + newPhone;
            } else {
                fixNumber = txtNewNumber.getText().toString();
            }
            paramObject.put("msisdn", userSession.getMsiSdn(getActivity()));
            paramObject.put("new_msisdn", fixNumber);
            encryptedBody = tripleDesJson.encrypt(paramObject.toString());

        } catch (JSONException e) {
        } catch (Exception e) {

        }
        System.out.println("asd setting change phone " + paramObject);
        userSession.setEncryptedBody(getActivity(), encryptedBody);
        userSession.setTripleDesKey(getActivity(), key);
        Call<String> call = apiInterface.change_msisdn(encryptedBody);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try {
                    //Toast.makeText(getActivity(),response.body().toString(),Toast.LENGTH_SHORT).show();
                    Setting.changeMsisdn changeMsisdn = new Gson().fromJson(tripleDesJson.decrypt(response.body()), Setting.changeMsisdn.class);
                    if (changeMsisdn.rm.equalsIgnoreCase("Success")) {
                        OtpVerificationFragment otpVerificationFragment = new OtpVerificationFragment();
                        Bundle extras = new Bundle();
                        extras.putInt("code", 4);
                        otpVerificationFragment.setArguments(extras);
                        FragmentManager fm = getActivity().getSupportFragmentManager();
                        FragmentTransaction ft = fm.beginTransaction();
                        ft.replace(R.id.frame_layout_setting, otpVerificationFragment);
                        ft.commit();
                    } else {
                        Snackbar.make(getView(), changeMsisdn.rm, Snackbar.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    System.out.println("asd catch " + e.toString());
                    Snackbar.make(getView(), "Cannot processing server's response !", Snackbar.LENGTH_SHORT).show();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

                Snackbar.make(getView(), "Oops, Something wrong please try again later", Snackbar.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }


}
