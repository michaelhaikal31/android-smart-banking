package products.dak.com.smartbank.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import products.dak.com.smartbank.R;
import products.dak.com.smartbank.utils.Respon;

public class ResponAdapter extends ArrayAdapter<Respon> {
    private static class ViewHolder {
        TextView tvHeader, tvValue;
    }
    public ResponAdapter(Context context, ArrayList<Respon> user) {
        super(context, R.layout.adapter_respon, user );
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Respon user = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag
        if (convertView == null) {
            // If there's no view to re-use, inflate a brand new view for row
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.adapter_respon, parent, false);
            viewHolder.tvHeader = (TextView) convertView.findViewById(R.id.tvHeader);
            viewHolder.tvValue = (TextView) convertView.findViewById(R.id.tvValue);
            convertView.setTag(viewHolder);
        } else {
            // View is being recycled, retrieve the viewHolder object from tag
            viewHolder = (ViewHolder) convertView.getTag();
        }
        // Populate the data from the data object via the viewHolder object
        // into the template view.
        viewHolder.tvHeader.setText(user.getHeader());
        viewHolder.tvValue.setText(user.getValue());

        // Return the completed view to render on screen
        return convertView;
    }
}
