package products.dak.com.smartbank.activities.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import products.dak.com.smartbank.R;
import products.dak.com.smartbank.activities.main.MainActivity;
import products.dak.com.smartbank.utils.Constant;

public class DialogAlertHome extends DialogFragment implements View.OnClickListener {
    private TextView tvValueDialog;
    public Context activity;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.activity = context;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        final Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(true);
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        final View view = inflater.inflate(R.layout.dialog_alert, container);
        view.findViewById(R.id.btnDialog).setOnClickListener(this);
        tvValueDialog = view.findViewById(R.id.tvValueDialog);
        tvValueDialog.setText(Constant.DATA_ALERT_RM);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnDialog :
                Intent intent = new Intent(getContext(), MainActivity.class);
                startActivity(intent);
                dismissAllowingStateLoss();
                break;
            default:
                throw new IllegalArgumentException("Unrecognized id");
        }
    }
    public void show(FragmentManager fragmentManager, String alert) {
        super.show(fragmentManager, alert);
    }
}
