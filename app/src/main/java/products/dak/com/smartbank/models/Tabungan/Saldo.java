package products.dak.com.smartbank.models.Tabungan;

public class Saldo {
    public String rc;
    public Data_saldo data;
    public String rm;
    public String msisdn;


    public Saldo(String rc, Data_saldo data, String rm, String msisdn) {
        this.rc = rc;
        this.data = data;
        this.rm = rm;
        this.msisdn = msisdn;
    }

    public String getRc() {
        return rc;
    }

    public Data_saldo getData() {
        return data;
    }

    public String getRm() {
        return rm;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public class Data_saldo{
        private String bal_inq_date;
        private String balance;
        private String name;
        private String rekening_no;

        public Data_saldo(String bal_inq_date, String balance, String name, String rekening_no) {
            this.bal_inq_date = bal_inq_date;
            this.balance = balance;
            this.name = name;
            this.rekening_no = rekening_no;
        }

        public String getBal_inq_date() {
            return bal_inq_date;
        }

        public String getBalance() {
            return balance;
        }

        public String getName() {
            return name;
        }

        public String getRekening_no() {
            return rekening_no;
        }

    }


}


