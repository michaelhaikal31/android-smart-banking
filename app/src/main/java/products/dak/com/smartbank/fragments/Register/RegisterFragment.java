package products.dak.com.smartbank.fragments.Register;

import android.app.ProgressDialog;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatDelegate;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import products.dak.com.smartbank.R;
import products.dak.com.smartbank.Sessions.UserSession;
import products.dak.com.smartbank.activities.main.FirstActivity;
import products.dak.com.smartbank.animations.MoveAnimation;
import products.dak.com.smartbank.fragments.Login.Login;
import products.dak.com.smartbank.fragments.Otp.OtpVerificationFragment;
import products.dak.com.smartbank.fragments.SplashScreenFragment;
import products.dak.com.smartbank.interfaces.ApiService;
import products.dak.com.smartbank.interfaces.FragmentChangeListener;
import products.dak.com.smartbank.services.RetrofitService;
import products.dak.com.smartbank.utils.Constant;
import products.dak.com.smartbank.utils.TripleDes;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class RegisterFragment extends Fragment implements FragmentChangeListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "step";
    private static final String ARG_PARAM2 = "full_name";
    private static final String ARG_PARAM3 = "msisdn";
    private static final String ARG_PARAM4 = "response";



    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String mParam3;
    private String mParam4;
    private Button btnRegister;
    private JSONObject paramObject;
    private String encryptedBody;
    private TripleDes tripleDesJSon,tripleDesBody;
    private EditText txtPhoneNumber;
    private String fixNumber;
    private ProgressDialog progressDialog;
    private EditText txtEmail,txtPin,txtConfirmPin;
    private TextView txtNamaLengkap;
    public RegisterFragment() {
        // Required empty public constructor
    }

    public static RegisterFragment newInstance(String param1, String param2,String param3,String param4) {
        RegisterFragment fragment = new RegisterFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        args.putString(ARG_PARAM3,param3);
        args.putString(ARG_PARAM4,param4);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            mParam3 = getArguments().getString(ARG_PARAM3);
            mParam4 = getArguments().getString(ARG_PARAM4);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = null;
        if(mParam1.equalsIgnoreCase("1")){
            v = inflater.inflate(R.layout.fragment_register_step_one,container,false);
            ((FirstActivity) getActivity()).getSupportActionBar().show();
            ((FirstActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
            final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
            upArrow.setColorFilter(getResources().getColor(R.color.colorDarkBlue), PorterDuff.Mode.SRC_ATOP);
            ((FirstActivity)getActivity()).getSupportActionBar().setHomeAsUpIndicator(upArrow);
            txtPhoneNumber = v.findViewById(R.id.txtPhoneNumber);
            btnRegister = v.findViewById(R.id.btnRegister);
            btnRegister.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    progressDialog = new ProgressDialog(getActivity());
                    progressDialog.setMessage("Please wait");
                    progressDialog.show();
                    checkMsisdn();
                    /*
                    */

                }
            });
        }else {
             v = inflater.inflate(R.layout.fragment_register_step_two, container, false);
            ((FirstActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
            upArrow.setColorFilter(getResources().getColor(R.color.colorDarkBlue), PorterDuff.Mode.SRC_ATOP);
            ((FirstActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(upArrow);
            txtNamaLengkap = v.findViewById(R.id.txtNamaLengkapRegister);
            txtEmail = v.findViewById(R.id.txtEmailRegister);
            txtPin = v.findViewById(R.id.txtPinRegister);
            txtConfirmPin = v.findViewById(R.id.txtConfirmPinRegister);
            btnRegister = v.findViewById(R.id.btnRegister);
            txtNamaLengkap.setText(mParam2);
           btnRegister.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   progressDialog = new ProgressDialog(getActivity());
                   progressDialog.setMessage("Please wait");
                   progressDialog.show();
                   sendRegistration();
               }
           });

        }
        return  v;
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if(mParam1.equalsIgnoreCase("1")){
            return MoveAnimation.create(MoveAnimation.RIGHT, enter, 300);
        }else{
            return MoveAnimation.create(MoveAnimation.LEFT, enter, 300);
        }
    }


    @Override
    public void replaceFragment(Fragment fragment) {

    }

    @Override
    public boolean onFragmentBackPressed() {
        if(mParam1.equalsIgnoreCase("2")) {
            RegisterFragment registerFragment = new RegisterFragment();
            Bundle extras = new Bundle();
            extras.putString("step", "1");
            registerFragment.setArguments(extras);
            FragmentManager fm = getActivity().getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.frame_layout1, registerFragment);
            ft.commit();
            return true;
        }else {
            SplashScreenFragment splashScreenFragment = new SplashScreenFragment();
            Bundle extras = new Bundle();
            extras.putString("step", "2");
            splashScreenFragment.setArguments(extras);
            FragmentManager fm = getActivity().getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.frame_layout1, splashScreenFragment);
            ft.commit();
            return true;
        }
    }

    private void checkMsisdn(){
        final String key = Constant.getKey(24);
        ApiService apiInterface = RetrofitService.getRetrofit(key).create(ApiService.class);
        paramObject = new JSONObject();
        try{
            tripleDesJSon = new TripleDes(key);
            fixNumber=String.valueOf(txtPhoneNumber.getText().toString().charAt(0));
            if(fixNumber.equalsIgnoreCase("0")){
                StringBuilder newPhone  = new StringBuilder(txtPhoneNumber.getText());
                newPhone.deleteCharAt(0);
                fixNumber = "62"+newPhone;
            }else{
                fixNumber = txtPhoneNumber.getText().toString();
            }
            System.out.println("asd "+fixNumber);
            paramObject.put("msisdn",fixNumber);
            encryptedBody = tripleDesJSon.encrypt(paramObject.toString());
        }catch (JSONException e){
            System.out.println("asd JSON Exception : "+e.toString());
        }catch (Exception e){
            System.out.println("asd encrypt error :"+e.toString());
        }

        System.out.println("asd plain "+paramObject);
        System.out.println("asd enc "+encryptedBody);

        final UserSession userSession = new UserSession();
        userSession.setEncryptedBody(getActivity(),encryptedBody);
        userSession.setTripleDesKey(getActivity(),key);

        Call<String> call = apiInterface.check_msisdn_register(encryptedBody);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try{
                    JSONObject obj = new JSONObject(tripleDesJSon.decrypt(response.body().toString()));
                    System.out.println("asd asd "+obj.getString("rc"));
                     if(obj.getString("rc").equalsIgnoreCase("00")){

                         final UserSession session = new UserSession();
                         userSession.setEncryptedBody(getActivity(),"");
                         userSession.setTripleDesKey(getActivity(),"");
                         session.setMsiSdn(getActivity(),fixNumber);
                         OtpVerificationFragment otpVerificationFragment = new OtpVerificationFragment();
                         Bundle extras  = new Bundle();
                         extras.putInt("code",3);
                         extras.putString("name",obj.getString("name"));
                         extras.putString("msisdn",fixNumber);
                         otpVerificationFragment.setArguments(extras);
                         FragmentManager fm = getActivity().getSupportFragmentManager();
                         FragmentTransaction ft = fm.beginTransaction();
                         ft.replace(R.id.frame_layout1,otpVerificationFragment);
                         ft.addToBackStack(null);
                         ft.commit();
                    }else{
                         Snackbar.make(getView(),obj.getString("rm"),Snackbar.LENGTH_SHORT).show();
                     }

                }catch (Exception e){
                    System.out.println("asd catch "+e.toString());
                    Snackbar.make(getView(),"Cannot processing server's response !",Snackbar.LENGTH_SHORT).show();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Snackbar.make(getView(),"Oops, Something wrong please try again later or check your internet connection",Snackbar.LENGTH_SHORT).show();
                System.out.println("asd failure "+t.toString());
                progressDialog.dismiss();
            }
        });

    }



    /*
     RegisterFragment registerFragment = new RegisterFragment();
                        Bundle extras  = new Bundle();
                        extras.putString("step","2");
                        extras.putString("full_name",obj.getString("name"));
                        registerFragment.setArguments(extras);
                        FragmentManager fm = getActivity().getSupportFragmentManager();
                        FragmentTransaction ft = fm.beginTransaction();
                        ft.replace(R.id.frame_layout1,registerFragment);
                        ft.commit();
     */


    private void sendRegistration(){
        final String key = Constant.getKey(24);
        ApiService apiInterface = RetrofitService.getRetrofit(key).create(ApiService.class);
        paramObject = new JSONObject();
        try{
            tripleDesBody = new TripleDes(Constant.KEY_DES_PIN);
            tripleDesJSon = new TripleDes(key);
            JSONObject response = new JSONObject(mParam4);
            System.out.println("asd respon "+response.toString());
            System.out.println("asd respon msisdn "+response.getString("msisdn"));
            paramObject.put("msisdn",response.getString("msisdn"));
            JSONObject data = response.getJSONObject("data");
            paramObject.put("cno",data.getString("cno"));
            paramObject.put("primary_name",data.getString("primary_name"));
            paramObject.put("email",tripleDesBody.encrypt(txtEmail.getText().toString().trim()));
            paramObject.put("pin",tripleDesBody.encrypt(txtPin.getText().toString().trim()));
            JSONArray data_tabungan = data.getJSONArray("data_tabungan");
            paramObject.put("data_tabungan",data_tabungan);
            encryptedBody = tripleDesJSon.encrypt(paramObject.toString());
        }catch (JSONException e){
            System.out.println("asd JSON Exception : "+e.toString());
        }catch (Exception e){
            System.out.println("asd encrypt error :"+e.toString());
        }
        System.out.println("asd plain "+paramObject);
        System.out.println("asd enc "+encryptedBody);
        Call<String> call = apiInterface.send_registration(encryptedBody);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try{
                    JSONObject obj = new JSONObject(tripleDesJSon.decrypt(response.body().toString()));
                    System.out.println("asd asd "+obj.getString("rc"));
                    if(obj.getString("rc").equalsIgnoreCase("00")){
                        Login login = new Login();
                        Bundle extras  = new Bundle();
                        extras.putString("jenis","1");
                        login.setArguments(extras);
                        FragmentManager fm = getActivity().getSupportFragmentManager();
                        FragmentTransaction ft = fm.beginTransaction();
                        Toast.makeText(getActivity(),"Registration Successfully",Toast.LENGTH_SHORT);
                        ft.replace(R.id.frame_layout1,login);
                        ft.commit();
                    }else{
                        Snackbar.make(getView(),obj.getString("rm"),Snackbar.LENGTH_SHORT).show();
                    }

                }catch (Exception e){
                    System.out.println("asd catch "+e.toString());
                    Snackbar.make(getView(),"Cannot processing server's response !",Snackbar.LENGTH_SHORT).show();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Snackbar.make(getView(),"Oops, Something wrong please try again later or check your internet connection",Snackbar.LENGTH_SHORT).show();
                System.out.println("asd failure "+t.toString());
                progressDialog.dismiss();
            }
        });

    }





}
