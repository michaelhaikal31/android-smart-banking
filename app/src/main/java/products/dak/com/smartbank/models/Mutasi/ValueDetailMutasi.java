package products.dak.com.smartbank.models.Mutasi;

import java.util.List;

public class ValueDetailMutasi {
    public String rc;
    public DetailMutasi data;
    public String rm;
    public String msisdn;
    public String trx_reff;

    public ValueDetailMutasi(String rc, DetailMutasi data, String rm, String msisdn, String trx_reff) {
        this.rc = rc;
        this.data = data;
        this.rm = rm;
        this.msisdn = msisdn;
        this.trx_reff = trx_reff;
    }

    public String getRc() {
        return rc;
    }

    public DetailMutasi getData() {
        return data;
    }

    public String getRm() {
        return rm;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public String getTrx_reff() {
        return trx_reff;
    }
}
