package products.dak.com.smartbank.interfaces;

public interface OnBackPressed {
    void onBackPressed();
}
