package products.dak.com.smartbank.utils;

public class ConstantTransaction {
    //JSON STATUS TRANSACTION
    public static String RC_STATUS = "rc";
    public static String RM_STATUS = "rm";
    public static String RC_SUCCESS = "00";
    public static String RM_SUCCESS = "Success";
    public static String DATA = "data";

    //BUNDLE TRANSACTION
    public static String BUNDLE_RM_RESULT = "rmResult";

    //JSON ID
    public static String MY_MSIDN = "msisdn";

    //JSON PULSA
    public static String MY_DESTINATION = "destination";
    public static String MY_DENOM = "denom";
    public static String MY_PROVIDER ="provider";

    //PEMBELIAN PULSA
    public static String MSISDN_PEMBELIAN_PULSA = "id_nasabah";
    public static String NUMBER_TUJUAN ="no_tujuan";
    public static String DENOM = "denom";
    public static String NOMINAL = "harga_jual";
    public static String PROVIDER = "provider";

}
