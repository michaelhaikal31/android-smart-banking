package products.dak.com.smartbank.fragments.Otp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import products.dak.com.smartbank.R;
import products.dak.com.smartbank.Sessions.UserSession;
import products.dak.com.smartbank.activities.main.FirstActivity;
import products.dak.com.smartbank.activities.main.MainActivity;
import products.dak.com.smartbank.fragments.Pembelian.DetailTransksiFragment;
import products.dak.com.smartbank.fragments.Register.RegisterFragment;
import products.dak.com.smartbank.fragments.Perbankan.DetailFragment;
import products.dak.com.smartbank.interfaces.ApiService;
import products.dak.com.smartbank.interfaces.AppBarControl;
import products.dak.com.smartbank.models.Otp;
import products.dak.com.smartbank.models.Setting.Setting;
import products.dak.com.smartbank.models.User.User;
import products.dak.com.smartbank.models.User.ValueUser;
import products.dak.com.smartbank.services.RetrofitService;
import products.dak.com.smartbank.utils.Constant;
import products.dak.com.smartbank.utils.ConstantTransaction;
import products.dak.com.smartbank.utils.Function;
import products.dak.com.smartbank.utils.TripleDes;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OtpVerificationFragment extends Fragment {
    private static final String ARG_PARAM1 = "code";
    private static final String ARG_PARAM2 = "msisdn";
    private static final String ARG_PARAM3 = "name";

    // TODO: Rename and change types of parameters
    private int mParam1;
    private String mParam2;
    private String mParam3;
    private PinEntryEditText otpEditText;
    private TextView text1, text2;
    private ProgressBar progressBar;
    private CountDownTimer countDownTimer;
    private TextView txtProgressBar, txt1, txt2, txt3;
    private Button btnProses;
    EditText txtPhoneNumber;
    private JSONObject paramObject;
    private User user = new User();
    ProgressDialog progressDialog;
    private String encryptedBody;
    private TripleDes tripleDesPin, tripleDesJSon;
    private TextView kirimUlangOtp;
    private AppBarControl appBarControl;

    public OtpVerificationFragment() {

    }



    /*
    EXPLANATION OF CODE PARAMETERS

    1 : otp login
    2 : otp transfer


     */


    public static OtpVerificationFragment newInstance(int param1, String param2, String param3) {
        OtpVerificationFragment fragment = new OtpVerificationFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        args.putString(ARG_PARAM3, param3);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getInt(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            mParam3 = getArguments().getString(ARG_PARAM3);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_otp_verification, container, false);
        kirimUlangOtp = v.findViewById(R.id.txtKirimUlangOtp);
        kirimUlangOtp.setVisibility(View.INVISIBLE);
        btnProses = v.findViewById(R.id.btnProsesOtp);
        progressBar = v.findViewById(R.id.progressBar);
        otpEditText = v.findViewById(R.id.txt_pin_entry);
        text2 = v.findViewById(R.id.textView2);
        TextView txtLeft = v.findViewById(R.id.textView16);
        TextView txtRight = v.findViewById(R.id.textView17);
        txt1 = v.findViewById(R.id.textView2);
        txt2 = v.findViewById(R.id.textView3);
        txt3 = v.findViewById(R.id.textView15);
        if (mParam1 == 1 || mParam1 == 3) {
            ((FirstActivity) getActivity()).getSupportActionBar().show();
            ((FirstActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
            upArrow.setColorFilter(getResources().getColor(R.color.colorDarkBlue), PorterDuff.Mode.SRC_ATOP);
            ((FirstActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(upArrow);
            final UserSession sessionUser = new UserSession();

            txtProgressBar = v.findViewById(R.id.txtProgressBar);
            txtLeft.setVisibility(View.INVISIBLE);
            txtRight.setVisibility(View.INVISIBLE);
            txtProgressBar.setVisibility(View.INVISIBLE);
            progressBar.setVisibility(View.INVISIBLE);

            if (mParam1 == 1) {
                txt3.setText("Masukan Pin anda ");
                text2.setText(sessionUser.getName(getActivity()));
                btnProses.setText("Login");
                btnProses.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        progressDialog = new ProgressDialog(getActivity());
                        progressDialog.setMessage("Please wait");
                        progressDialog.show();
                        login();
                    }
                });

            } else {
                text2.setText(mParam3);
                btnProses.setText("Register");
                btnProses.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        progressDialog = new ProgressDialog(getActivity());
                        progressDialog.setMessage("Please wait");
                        progressDialog.show();
                        otp_register();
                    }
                });
            }

        } else {
            appBarControl = (AppBarControl) getActivity();
            appBarControl.setClose();
            txt1.setVisibility(View.INVISIBLE);
            txt2.setVisibility(View.INVISIBLE);
            txt3.setText("Masukan OTP yang kami kirim melalui sms dibawah ini !");
            txtProgressBar = v.findViewById(R.id.txtProgressBar);
            startCountdown();

            //set tombol dan textview
            switch (mParam1) {
                case 2:
                    btnProses.setText("Transfer");
                    btnProses.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            progressDialog = new ProgressDialog(getActivity());
                            progressDialog.setMessage("Please wait");
                            progressDialog.show();
                            otp_transfer();
                        }
                    });
                    break;
                case 3:
                    btnProses.setText("Register");
                    btnProses.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            progressDialog = new ProgressDialog(getActivity());
                            progressDialog.setMessage("Please wait");
                            progressDialog.show();
                            System.out.println("asd asd asd");
                            otp_register();
                        }
                    });
                    break;
                case 4:
                    btnProses.setText("Konfirmasi");
                    btnProses.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            progressDialog = new ProgressDialog(getActivity());
                            progressDialog.setMessage("Please wait");
                            progressDialog.show();
                            otp_change_phone();
                        }
                    });
                    break;
                case 5:
                    btnProses.setText("Konfirmasi");
                    btnProses.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            progressDialog = new ProgressDialog(getActivity());
                            progressDialog.setMessage("Please wait");
                            progressDialog.show();
                            otp_change_pin();
                        }
                    });
                    break;
                case 6: btnProses.setText("Konfirmasi Pembayaran");
                    btnProses.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            progressDialog = new ProgressDialog(getActivity());
                            progressDialog.setMessage("Please wait");
                            progressDialog.show();
                            otp_isi_pulsa();
                        }
                    });
                    break;

            }

        }

        //startCountdown();
        return v;
    }

    private void startCountdown() {
        countDownTimer = new CountDownTimer(60000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                int second = (int) ((millisUntilFinished / 1000));
                //txtProgressBar.setText(second);
                txtProgressBar.setText(String.valueOf(second));

                progressBar.setProgress(second);
            }

            @Override
            public void onFinish() {
                kirimUlangOtp.setVisibility(View.VISIBLE);
                kirimUlangOtp.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        progressDialog = new ProgressDialog(getActivity());
                        progressDialog.setMessage("Sending new OTP");
                        progressDialog.show();
                        resendOtp();
                        startCountdown();
                        kirimUlangOtp.setVisibility(View.INVISIBLE);
                    }
                });
            }
        };

        countDownTimer.start();

    }

    private void login() {
        final String key = Constant.getKey(24);
        ApiService apiInterface = RetrofitService.getRetrofit(key).create(ApiService.class);
        paramObject = new JSONObject();
        try {
            tripleDesPin = new TripleDes(Constant.KEY_DES_PIN);
            tripleDesJSon = new TripleDes(key);
            paramObject.put("msisdn", mParam2);
            paramObject.put("pin", tripleDesPin.encrypt(otpEditText.getText().toString()));
            encryptedBody = tripleDesJSon.encrypt(paramObject.toString());
        } catch (JSONException e) {
        } catch (Exception e) {

        }
        System.out.println("asd login " + paramObject);
        Call<String> call = apiInterface.login(encryptedBody);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try {
                    //Toast.makeText(getActivity(),response.body().toString(),Toast.LENGTH_SHORT).show();;
                    ValueUser valueUser = new Gson().fromJson(tripleDesJSon.decrypt(response.body()), ValueUser.class);
                    if (valueUser.getRc().equalsIgnoreCase("00")) {
                        user = valueUser.getData();
                        final UserSession session = new UserSession();
                        session.setName(getActivity(), valueUser.getData().name);
                        session.setMsiSdn(getActivity(), mParam2);
                        Intent i = new Intent(getActivity(), MainActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                        getActivity().finish();
                    } else {
                        Snackbar.make(getView(), valueUser.getRm(), Snackbar.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    System.out.println("asd catch " + e.toString());

                    Snackbar.make(getView(), "Cannot processing server's response !", Snackbar.LENGTH_SHORT).show();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Snackbar.make(getView(), "Oops, Something wrong please try again later", Snackbar.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    private void otp_transfer() {
        final String key = Constant.getKey(24);
        ApiService apiInterface = RetrofitService.getRetrofit(key).create(ApiService.class);
        paramObject = new JSONObject();
        final UserSession userSession = new UserSession();
        try {
            tripleDesJSon = new TripleDes(key);
            tripleDesPin = new TripleDes(Constant.KEY_DES_PIN);
            paramObject.put("msisdn", userSession.getMsiSdn(getActivity()));
            paramObject.put("otp", tripleDesPin.encrypt(otpEditText.getText().toString()));
            encryptedBody = tripleDesJSon.encrypt(paramObject.toString());
        } catch (JSONException e) {
            System.out.println("asd JSON Exception : " + e.toString());
        } catch (Exception e) {
            System.out.println("asd encrypt error :" + e.toString());
        }

        System.out.println("asd enc " + paramObject);
        Call<String> call = apiInterface.transfer_otp(encryptedBody);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try {
                    //System.out.println("asd respon "+tripleDes.decrypt(response.toString()));
                    System.out.println("asd asd message " + response.message());
                    System.out.println("asd asd body " + tripleDesJSon.decrypt(response.body()));
                    Otp otp = new Gson().fromJson(tripleDesJSon.decrypt(response.body()), Otp.class);
                    if (otp.getRc().equalsIgnoreCase("00")) {
                        userSession.setTripleDesKey(getActivity(), "");
                        userSession.setEncryptedBody(getActivity(), "");
                        DetailFragment detailFragment = new DetailFragment();
                        Bundle extras = new Bundle();
                        extras.putInt("type", 2);
                        extras.putString("mParam2", tripleDesJSon.decrypt(response.body()));
                        detailFragment.setArguments(extras);
                        FragmentManager fm = getActivity().getSupportFragmentManager();
                        FragmentTransaction ft = fm.beginTransaction();
                        ft.replace(R.id.frame_layout_super, detailFragment);
                        ft.commit();
                    } else {
                        Snackbar.make(getView(), otp.getRm(), Snackbar.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    System.out.println("asd catch " + e.toString());
                    Snackbar.make(getView(), "Cannot processing server's response otp !", Snackbar.LENGTH_SHORT).show();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Snackbar.make(getView(), "Oops, Something wrong please try again later or check your internet connection", Snackbar.LENGTH_SHORT).show();
                System.out.println("asd failure " + t.toString());
                progressDialog.dismiss();
            }
        });


    }

    private void otp_register() {
        final String key = Constant.getKey(24);
        ApiService apiInterface = RetrofitService.getRetrofit(key).create(ApiService.class);
        paramObject = new JSONObject();
        final UserSession userSession = new UserSession();
        try {
            tripleDesJSon = new TripleDes(key);
            tripleDesPin = new TripleDes(Constant.KEY_DES_PIN);
            paramObject.put("msisdn", userSession.getMsiSdn(getActivity()));
            paramObject.put("otp", tripleDesPin.encrypt(otpEditText.getText().toString()));
            encryptedBody = tripleDesJSon.encrypt(paramObject.toString());
        } catch (JSONException e) {
            System.out.println("asd JSON Exception : " + e.toString());
        } catch (Exception e) {
            System.out.println("asd encrypt error :" + e.toString());
        }

        System.out.println("asd enc " + paramObject);
        Call<String> call = apiInterface.register_otp(encryptedBody);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try {
                    //System.out.println("asd respon "+tripleDes.decrypt(response.toString()));
                    JSONObject obj = new JSONObject(tripleDesJSon.decrypt(response.body().toString()));

                    if (obj.getString("rc").equalsIgnoreCase("00")) {
                        RegisterFragment registerFragment = new RegisterFragment();
                        Bundle extras = new Bundle();
                        extras.putString("step", "2");
                        extras.putString("full_name", obj.getJSONObject("data").getString("primary_name"));
                        extras.putString("response", obj.toString());
                        registerFragment.setArguments(extras);
                        FragmentManager fm = getActivity().getSupportFragmentManager();
                        FragmentTransaction ft = fm.beginTransaction();
                        ft.replace(R.id.frame_layout1, registerFragment);
                        ft.commit();
                    } else {
                        Snackbar.make(getView(), obj.getString("rm"), Snackbar.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    System.out.println("asd catch " + e.toString());
                    Snackbar.make(getView(), "Cannot processing server's response !", Snackbar.LENGTH_SHORT).show();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Snackbar.make(getView(), "Oops, Something wrong please try again later or check your internet connection", Snackbar.LENGTH_SHORT).show();
                System.out.println("asd failure " + t.toString());
                progressDialog.dismiss();
            }
        });


    }

    private void otp_change_pin() {
        final String key = Constant.getKey(24);
        ApiService apiInterface = RetrofitService.getRetrofit(key).create(ApiService.class);
        paramObject = new JSONObject();
        final UserSession userSession = new UserSession();
        try {
            tripleDesPin = new TripleDes(Constant.KEY_DES_PIN);
            tripleDesJSon = new TripleDes(key);
            if (mParam2 == null) {
                paramObject.put("msisdn", userSession.getMsiSdn(getActivity()));
            } else {
                paramObject.put("msisdn", mParam2);
            }

            paramObject.put("otp", tripleDesPin.encrypt(otpEditText.getText().toString()));
            encryptedBody = tripleDesJSon.encrypt(paramObject.toString());

        } catch (JSONException e) {
        } catch (Exception e) {
        }

        System.out.println("asd " + paramObject.toString());

        Call<String> call = apiInterface.conf_change_pin(encryptedBody);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try {
                    //Toast.makeText(getActivity(),response.body().toString(),Toast.LENGTH_SHORT).show();;
                    Setting.changePin changePin = new Gson().fromJson(tripleDesJSon.decrypt(response.body()), Setting.changePin.class);
                    if (changePin.rm.equalsIgnoreCase("Success")) {
                        userSession.setEncryptedBody(getActivity(), "");
                        userSession.setTripleDesKey(getActivity(), "");
                        Toast.makeText(getActivity(), changePin.rm, Toast.LENGTH_SHORT).show();
                        userSession.clearSession(getActivity());
                        Intent i = new Intent(getActivity(), FirstActivity.class);
                        startActivity(i);
                        getActivity().finish();

                    } else {
                        Snackbar.make(getView(), changePin.rm, Snackbar.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    System.out.println("asd catch " + e.toString());
                    Snackbar.make(getView(), "Cannot processing server's response !", Snackbar.LENGTH_SHORT).show();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Snackbar.make(getView(), "Oops, Something wrong please try again later", Snackbar.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    private void otp_change_phone() {
        final String key = Constant.getKey(24);
        ApiService apiInterface = RetrofitService.getRetrofit(key).create(ApiService.class);
        paramObject = new JSONObject();
        final UserSession userSession = new UserSession();
        try {
            tripleDesPin = new TripleDes(Constant.KEY_DES_PIN);
            tripleDesJSon = new TripleDes(key);
            paramObject.put("msisdn", userSession.getMsiSdn(getActivity()));
            paramObject.put("otp", tripleDesPin.encrypt(otpEditText.getText().toString()));
            encryptedBody = tripleDesJSon.encrypt(paramObject.toString());
        } catch (JSONException e) {
        } catch (Exception e) {

        }
        System.out.println("asd " + paramObject.toString());
        Call<String> call = apiInterface.conf_change_msisdn(encryptedBody);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try {
                    //Toast.makeText(getActivity(),response.body().toString(),Toast.LENGTH_SHORT).show();;
                    Setting.changeMsisdn changeMsisdn = new Gson().fromJson(tripleDesJSon.decrypt(response.body()), Setting.changeMsisdn.class);
                    if (changeMsisdn.rm.equalsIgnoreCase("Success")) {
                        Toast.makeText(getActivity(), changeMsisdn.rm, Toast.LENGTH_SHORT).show();
                        userSession.setTripleDesKey(getActivity(), "");
                        userSession.setEncryptedBody(getActivity(), "");
                        userSession.clearSession(getActivity());
                        Intent i = new Intent(getActivity(), FirstActivity.class);
                        startActivity(i);
                        getActivity().finish();
                    } else {
                        Snackbar.make(getView(), changeMsisdn.rm, Snackbar.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    System.out.println("asd catch " + e.toString());
                    Snackbar.make(getView(), "Cannot processing server's response !", Snackbar.LENGTH_SHORT).show();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Snackbar.make(getView(), "Oops, Something wrong please try again later", Snackbar.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    private void otp_isi_pulsa() {
        final String key = Constant.getKey(24);
        ApiService apiInterface = RetrofitService.getRetrofitDev(key).create(ApiService.class);
        paramObject = new JSONObject();
        final UserSession userSession = new UserSession();
        try {
            paramObject.put("msisdn", userSession.getMsiSdn(getActivity()));
            paramObject.put("otp",otpEditText.getText().toString());
            paramObject.put("jenis_tabungan",userSession.getJenisTabungan(getContext()));
        } catch (JSONException e) {
            e.toString();
        }
        Call<String> call = apiInterface.posting_Pulsa(paramObject.toString());
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try {
                    JSONObject object = new JSONObject(response.body());
                    if(object.getString(ConstantTransaction.RC_STATUS).equalsIgnoreCase(ConstantTransaction.RC_SUCCESS)){
                        DetailTransksiFragment detailTransksiFragment = new DetailTransksiFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString(ConstantTransaction.BUNDLE_RM_RESULT, response.body());
                        detailTransksiFragment.setArguments(bundle);
                        FragmentManager fm = getActivity().getSupportFragmentManager();
                        FragmentTransaction ft = fm.beginTransaction();
                        ft.replace(R.id.frame_layout_super, detailTransksiFragment);
                        ft.commit();
                    }else {
                        Function.showAlert(getActivity(),object.getString("rm"));
//                        final DialogAlert dialogAlert = new DialogAlert();
//                        dialogAlert.show(getFragmentManager(),"Alert");
//                        Constant.DATA_ALERT_RM = object.getString("rm");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                System.out.println(t.getMessage());
                progressDialog.dismiss();
            }
        });

    }

    private void resendOtp() {
        final UserSession userSession = new UserSession();
        try {
            tripleDesJSon = new TripleDes(userSession.getTripleDesKey(getActivity()));
        } catch (Exception e) {
        }
        ApiService apiInterface = RetrofitService.getRetrofit(userSession.getTripleDesKey(getActivity())).create(ApiService.class);
        System.out.println("asd resend " + userSession.getEncryptedBody(getActivity()));
        Call<String> call = null;


        //mengcek dari parameter apakah jenis proses otp yang dibutuhkan

        switch (mParam1) {
            case 2:
                call = apiInterface.conf_transfer(userSession.getEncryptedBody(getActivity()));
                break;
            case 3:
                call = apiInterface.register_otp(userSession.getEncryptedBody(getActivity()));
                break;
            case 4:
                call = apiInterface.change_msisdn(userSession.getEncryptedBody(getActivity()));
                break;
            case 5:
                call = apiInterface.change_pin(userSession.getEncryptedBody(getActivity()));
                break;
            case 6:
                call = apiInterface.send_OtpPulsa(userSession.getMsiSdn(getActivity()));
                break;

        }
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try {
                    //Toast.makeText(getActivity(),response.body().toString(),Toast.LENGTH_SHORT).show();;
                    Setting.changeMsisdn changeMsisdn = new Gson().fromJson(tripleDesJSon.decrypt(response.body()), Setting.changeMsisdn.class);
                    if (changeMsisdn.rm.equalsIgnoreCase("Success")) {
                        Snackbar.make(getView(), "We have sent icon_change_pin new code via SMS", Snackbar.LENGTH_SHORT).show();
                    } else {
                        Snackbar.make(getView(), changeMsisdn.rm, Snackbar.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    System.out.println("asd catch " + e.toString());
                    Snackbar.make(getView(), "Cannot processing server's response !", Snackbar.LENGTH_SHORT).show();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                System.out.println("asd failure " + t.toString() + " | " + t.getMessage());
                Snackbar.make(getView(), "Oops, Something wrong please try again later", Snackbar.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }


}
