package products.dak.com.smartbank.models;

public class BaseResponse {
    private String rc;
    private String rm;
    private String msisdn;

    public BaseResponse() {
    }

    public BaseResponse(String rc, String rm, String msisdn) {
        this.rc = rc;
        this.rm = rm;
        this.msisdn = msisdn;
    }

    public String getRc() {
        return rc;
    }
    public String getRm() {
        return rm;
    }

    public String getMsisdn() {
        return msisdn;
    }
}
