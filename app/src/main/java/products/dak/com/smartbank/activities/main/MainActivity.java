package products.dak.com.smartbank.activities.main;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import io.fabric.sdk.android.Fabric;
import products.dak.com.smartbank.Sessions.UserSession;
import products.dak.com.smartbank.activities.SettingActivity;
import products.dak.com.smartbank.fragments.Perbankan.DashboardFragment;
import products.dak.com.smartbank.fragments.Product.RekeningGiroFragment;
import products.dak.com.smartbank.fragments.Product.DepositoBerjangkaFragment;
import products.dak.com.smartbank.interfaces.ApiService;
import products.dak.com.smartbank.interfaces.FragmentChangeListener;
import products.dak.com.smartbank.R;
import products.dak.com.smartbank.interfaces.SnackBar;
import products.dak.com.smartbank.models.Tabungan.Saldo;
import products.dak.com.smartbank.services.RetrofitService;
import products.dak.com.smartbank.utils.Constant;
import products.dak.com.smartbank.utils.TripleDes;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, FragmentChangeListener, DrawerLayout.DrawerListener, SnackBar {
    private AHBottomNavigation bottomNavigation;
    private Button btnSetting;
    private ProgressBar progressBar;
    private JSONObject paramObject;
    private TripleDes tripleDesJson;
    private String encryptedBody;
    private TextView saldo, userName, msisdn, textSnackBar;
    private Call<String> call;
    private DrawerLayout drawer;
    private ImageView btnCloseDrawer, btnCloseDrawer2, imgSnackBar;
    private Button btnLogout;
    private Toolbar toolbar;
    private LinearLayout linearLayout;
    private ConstraintLayout constraintLayout;
    private NavigationView navigationView, navigationView2;
    private Boolean Temp = false;

    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        overridePendingTransition(R.anim.pull_in_from_left, R.anim.hold);
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }

        /*disable screenshoot
        if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        }
        */
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        bottomNavigation = (AHBottomNavigation) findViewById(R.id.bottom_navigation);
        setSupportActionBar(toolbar);
        //bottom navigation
        AHBottomNavigationItem item1 = new AHBottomNavigationItem("BPR Modern", R.drawable.icon_rekening_tabungan);
        bottomNavigation.addItem(item1);

        AHBottomNavigationItem item2 = new AHBottomNavigationItem("Tabungan Ku", R.drawable.icon_tabungan_giro);
        bottomNavigation.addItem(item2);

        AHBottomNavigationItem item3 = new AHBottomNavigationItem("Tabungan Simpel", R.drawable.icon_deposito_berjangka);
        bottomNavigation.addItem(item3);

        //item1 = new AHBottomNavigationItem("Tabungan Karyawan",R.drawable.icon_obligasi);
        //bottomNavigation.addItem(item1);

        AHBottomNavigationItem item4 = new AHBottomNavigationItem("More", R.drawable.icon_more);
        bottomNavigation.addItem(item4);


        bottomNavigation.setCurrentItem(0);

        bottomNavigation.setDefaultBackgroundColor(Color.parseColor("#FEFEFE"));
        bottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW);
        bottomNavigation.setAccentColor(Color.parseColor("#2b388f"));
        bottomNavigation.setInactiveColor(Color.parseColor("#747474"));
        bottomNavigation.setForceTint(true);


        final UserSession userSession = new UserSession();
        userSession.setJenisTabungan(MainActivity.this, "tabungan bpr modern");
        bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {
                //drawer.openDrawer(GravityCompat.END);
                final DashboardFragment dashboardFragment = new DashboardFragment();
                switch (position) {
                    case 0:
                        userSession.setJenisTabungan(MainActivity.this, "tabungan bpr modern");
                        addFragment(dashboardFragment);
                        showSnackBar(false, null);
                        break;
                    case 1:
                        userSession.setJenisTabungan(MainActivity.this, "tabungan bpr modern");
                        addFragment(new DepositoBerjangkaFragment());
                        break;
                    case 2:
                        userSession.setJenisTabungan(MainActivity.this, "tabungan bpr modern");
                        addFragment(new RekeningGiroFragment());
                        break;
                    case 3:
                        drawer.openDrawer(GravityCompat.END);
                        Temp = true;
                        break;
                    default:
                        break;
                }
                return true;

            }
        });

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        drawer.addDrawerListener(this);
        toggle.syncState();


        navigationView = findViewById(R.id.nav_view);
        navigationView2 = findViewById(R.id.nav_view2);

        saldo = navigationView.findViewById(R.id.txtSaldo);
        userName = navigationView.findViewById(R.id.txtInfoUserName);
        msisdn = navigationView.findViewById(R.id.txtInfoMsisdn);
        saldo.setText("...");
        userName.setText("...");
        msisdn.setText("...");

        progressBar = navigationView.findViewById(R.id.pb_load_saldo);

        btnCloseDrawer2 = navigationView2.findViewById(R.id.btnCloseDrawerEnd);
        btnCloseDrawer2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.closeDrawer(GravityCompat.END);
            }
        });
        btnCloseDrawer = navigationView.findViewById(R.id.btnCloseDrawer);
        btnCloseDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        btnSetting = navigationView.findViewById(R.id.btnSetting);
        btnLogout = navigationView.findViewById(R.id.btnLogout);
        btnSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, SettingActivity.class);
                i.putExtra("menu", "toMenuSetting");
                startActivity(i);
            }
        });

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
                alert.setTitle("Logout");
                alert.setMessage("Are you sure ?");
                alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(MainActivity.this, FirstActivity.class);
                        startActivity(intent);
                        finish();
                        UserSession userSession = new UserSession();
                        userSession.clearSession(MainActivity.this);
                    }
                });
                alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        return;
                    }
                });
                alert.create();
                alert.show();


            }
        });

        navigationView.setNavigationItemSelectedListener(this);

        // set inisialisasi SnackBar
        constraintLayout = findViewById(R.id.backgroundSnackBar);
        linearLayout = findViewById(R.id.includesnackbar);
        textSnackBar = findViewById(R.id.textsnackbar);
        imgSnackBar = (ImageView) findViewById(R.id.imageSnackBar);
        // set the default snackbar on the tab menu
        showSnackBar(false, null);

        DashboardFragment dashboardFragment = new DashboardFragment();
        addFragment(dashboardFragment);

    }


    @Override
    public void onBackPressed() {

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (drawer.isDrawerOpen(GravityCompat.END)) {
                drawer.closeDrawer(GravityCompat.END);
            } else {
                finish();
            }
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void check_saldo() {
        final String key = Constant.getKey(24);
        ApiService apiInterface = RetrofitService.getRetrofit(key).create(ApiService.class);
        paramObject = new JSONObject();
        final UserSession userSession = new UserSession();
        try {
            tripleDesJson = new TripleDes(key);
            paramObject.put("msisdn", userSession.getMsiSdn(this));
            paramObject.put("jenis_tabungan", "tabungan bpr modern");
            encryptedBody = tripleDesJson.encrypt(paramObject.toString());
        } catch (JSONException e) {

        } catch (Exception e) {

        }

        System.out.println("asd " + paramObject.toString());
        call = apiInterface.cek_saldo(encryptedBody);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try {
                    //Toast.makeText(getActivity(),response.body().toString(),Toast.LENGTH_SHORT).show();
                    Saldo valueSaldo = new Gson().fromJson(tripleDesJson.decrypt(response.body()), Saldo.class);
                    if (valueSaldo.rm.equalsIgnoreCase("Success")) {
                        saldo.setText("Rp." + valueSaldo.data.getBalance());
                        userName.setText(valueSaldo.data.getName());
                        msisdn.setText(valueSaldo.data.getRekening_no());
                        saldo.setVisibility(View.VISIBLE);
                        userName.setVisibility(View.VISIBLE);
                        saldo.setVisibility(View.VISIBLE);
                    } else {
                        saldo.setText("Unable to connect");
                        saldo.setVisibility(View.VISIBLE);
                    }

                } catch (Exception e) {
                    saldo.setText("Unable to connect");
                    saldo.setVisibility(View.VISIBLE);
                }
                progressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                saldo.setText("Unable to connect");
                saldo.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.INVISIBLE);
            }
        });
    }

    private void addFragment(Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        //ft.add(R.id.frame_layout,fragment);
        ft.replace(R.id.frame_layout, fragment);
        ft.addToBackStack(null);
        ft.commit();
    }

    @Override
    public void replaceFragment(Fragment fragment) {
        addFragment(fragment);
    }

    @Override
    public boolean onFragmentBackPressed() {
        return false;
    }

    @Override
    public void onDrawerSlide(@NonNull View view, float v) {
    }

    @Override
    public void onDrawerOpened(@NonNull View view) {
        progressBar.setVisibility(View.VISIBLE);
        check_saldo();
    }

    @Override
    public void onDrawerClosed(@NonNull View view) {
        if (view.findViewById(R.id.nav_view2) != null) {
            if (Temp == true) {
                bottomNavigation.setCurrentItem(0);
                bottomNavigation.setCurrentItem(0, true);
                Temp = false;
            } else {
                bottomNavigation.refresh();
            }
        }else {

            if (call != null) {
                System.out.println("asd  masuk");
                call.cancel();
            }
        }


    }

    @Override
    public void onDrawerStateChanged(int i) {

    }

    @Override
    public void showSnackBar(boolean show, String name) {
        if (show == true) {
            linearLayout.setVisibility(View.VISIBLE);
            textSnackBar.setText(getString(R.string.text_snakbar) + " " + name);
            if (name == "Rekening Giro") {
                imgSnackBar.setImageResource(R.drawable.icon_tabungan_giro);
                constraintLayout.setBackgroundResource(R.drawable.background_rekening_giro);
            } else {
                imgSnackBar.setImageResource(R.drawable.icon_obligasi);
                constraintLayout.setBackgroundResource(R.drawable.background_deposito_berjangka);
            }
        } else {
            linearLayout.setVisibility(View.GONE);

        }


    }
}
