package products.dak.com.smartbank.fragments.Pembelian;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import products.dak.com.smartbank.R;
import products.dak.com.smartbank.Sessions.UserSession;
import products.dak.com.smartbank.adapters.ResponAdapter;
import products.dak.com.smartbank.fragments.Otp.OtpVerificationFragment;
import products.dak.com.smartbank.interfaces.ApiService;
import products.dak.com.smartbank.services.RetrofitService;
import products.dak.com.smartbank.utils.Constant;
import products.dak.com.smartbank.utils.ConstantTransaction;
import products.dak.com.smartbank.utils.Respon;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class KonfirmasiPembelianFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;


    private ListView list_pulsa;
    private View view;
    private Button btnKofirmasi;
    private ArrayList<Respon> mylist = new ArrayList<>();

    public KonfirmasiPembelianFragment() {
        // Required empty public constructor
    }

    public static KonfirmasiPembelianFragment newInstance(String param1, String param2) {
        KonfirmasiPembelianFragment fragment = new KonfirmasiPembelianFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_konfirmasi_pembelian, container, false);
        InitUI();
        return view;

    }

    private void InitUI() {
        btnKofirmasi = view.findViewById(R.id.btnKomfirmasi);
        list_pulsa = view.findViewById(R.id.list_pulsa);
        mylist.clear();
        JSONObject getid;
        String[] a = new String[]{"Msisdn", "No Tujuan", "Pulsa", "Nominal", "Provider", "AmounTransaction"};
        for (int i = 0; i < Constant.DATA_RESULT.length() - 1; i++) {
            try {
                getid = Constant.DATA_RESULT.getJSONObject(i);
                mylist.add(new Respon(a[i], getid.getString("value")));
            }catch (Exception e){
                System.out.println(e.toString());
            }
        }
        ResponAdapter adapter = new ResponAdapter(getActivity(), mylist);
        list_pulsa.setAdapter(adapter);
        btnKofirmasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isi_pulsa();
            }
        });
    }

    private void isi_pulsa() {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait");
        progressDialog.show();

        final String key = Constant.getKey(24);
        ApiService apiService = RetrofitService.getRetrofitDev(key).create(ApiService.class);
        JSONObject jsonObject = new JSONObject();
        final UserSession userSession = new UserSession();
        try {
            jsonObject.put(ConstantTransaction.MY_MSIDN, userSession.getMsiSdn(getContext()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        Call<String> call = apiService.send_OtpPulsa(jsonObject.toString());
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try {
                    JSONObject object = new JSONObject(response.body().toString());
                    if (object.getString("rc").equalsIgnoreCase("00")) {

                        OtpVerificationFragment otpVerificationFragment = new OtpVerificationFragment();
                        Bundle extras = new Bundle();
                        extras.putInt("code", 6);
                        otpVerificationFragment.setArguments(extras);
                        FragmentManager fm = getActivity().getSupportFragmentManager();
                        FragmentTransaction ft = fm.beginTransaction();
                        ft.replace(R.id.frame_layout_super, otpVerificationFragment);
                        ft.commit();

//                        DetailTransksiFragment detailTransksiFragment = new DetailTransksiFragment();
//                        Bundle bundle = new Bundle();
//                        bundle.putString(ConstantTransaction.BUNDLE_RM_RESULT, "Success");
//                        detailTransksiFragment.setArguments(bundle);
//                        FragmentManager fm = getActivity().getSupportFragmentManager();
//                        FragmentTransaction ft = fm.beginTransaction();
//                        ft.replace(R.id.frame_layout_super, detailTransksiFragment);
//                        ft.commit();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

}
