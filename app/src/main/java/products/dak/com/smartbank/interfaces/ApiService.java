package products.dak.com.smartbank.interfaces;



import products.dak.com.smartbank.models.Setting.Setting;
import products.dak.com.smartbank.models.Tabungan.Saldo;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;


public interface ApiService {


    @POST("check_msisdn")
    Call<String> check_msisdn(@Body String body);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("login")
    Call<String> login(@Body String body);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("change_pin")
    Call<String> change_pin(@Body String body);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("conf_change_pin")
    Call<String> conf_change_pin(@Body String body);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("change_msisdn")
    Call<String> change_msisdn(@Body String body);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("conf_change_msisdn")
    Call<String> conf_change_msisdn(@Body String body);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("delete_account")
    Call<String>delete_account(@Body String body);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("inq_balance")
    Call<String>cek_saldo(@Body String body);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("inq_trx")
    Call<String>cek_mutasi(@Body String body);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("inq_dtl_trx")
    Call<String>get_DetailMutasi(@Body String body);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("transfer_check")
    Call<String>cek_noRekening(@Body String body);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("transfer_inquiry")
    Call<String>transfer(@Body String body);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("transfer_conf_pin")
    Call<String>conf_transfer(@Body String body);


    @Headers({
            "Content-Type: application/json"
    })
    @POST("transfer_posting")
    Call<String>transfer_otp(@Body String body);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("register_inq")
    Call<String>check_msisdn_register(@Body String body);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("register_post")
    Call<String>register_otp(@Body String body);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("registration")
    Call<String>send_registration(@Body String body);

    @POST("list_denom_telco")
    Call<String>send_pulsa(@Body String body);

    @POST("telcoprepaid_inquiry")
    Call<String>hargajual_pulsa(@Body String body);

    @POST("telcoprepaid_conf")
    Call<String>send_OtpPulsa(@Body String body);

    @POST("telcoprepaid_posting")
    Call<String>posting_Pulsa(@Body String body);

}
