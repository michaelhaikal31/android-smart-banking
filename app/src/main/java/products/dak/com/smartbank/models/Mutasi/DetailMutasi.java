package products.dak.com.smartbank.models.Mutasi;

public class DetailMutasi {
    private String amount;
    private String trx_dtl;
    private String date_time_trx;
    private String destination;
    private String trx_type;
    private String trx;
    private String trx_reff;
    private String status;

    public DetailMutasi(String amount, String trx_dtl, String date_time_trx, String destination, String trx_type, String trx, String trx_reff, String status) {
        this.amount = amount;
        this.trx_dtl = trx_dtl;
        this.date_time_trx = date_time_trx;
        this.destination = destination;
        this.trx_type = trx_type;
        this.trx = trx;
        this.trx_reff = trx_reff;
        this.status = status;
    }

    public String getAmount() {
        return amount;
    }

    public String getTrx_dtl() {
        return trx_dtl;
    }

    public String getDate_time_trx() {
        return date_time_trx;
    }

    public String getDestination() {
        return destination;
    }

    public String getTrx_type() {
        return trx_type;
    }

    public String getTrx() {
        return trx;
    }

    public String getTrx_reff() {
        return trx_reff;
    }

    public String getStatus() {
        return status;
    }
}
